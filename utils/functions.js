
import { withRouter } from 'next/router'
import Router from 'next/router';


export function _getQueryVariable(variable)
{
  var query = window.location.search.substring(1);
  // console.log(query)//"app=article&act=news_content&aid=160990"
  var vars = query.split("&");
  // console.log(vars) //[ 'app=article', 'act=news_content', 'aid=160990' ]
  for (var i=0;i<vars.length;i++) {
  var pair = vars[i].split("=");
  // console.log(pair)//[ 'app', 'article' ][ 'act', 'news_content' ][ 'aid', '160990' ] 
  if(pair[0] == variable){return pair[1];}
  }
  return(false);
}

export function _buildFormData(formData, data , parentKey){
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
      Object.keys(data).forEach(key => {
        //   if (parentKey == 'profile_images') {
        //     this.buildFormData(formData, data[key], parentKey ? `${parentKey}` : key);
        //   } else {
            _buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
        //   }
      });
    } else {
      const value = data == null ? '' : data;
      formData.append(parentKey, value);
    }
  }
  
  
export function _jsonToFormData(data) {
    const formData = new FormData();
    _buildFormData(formData, data, null);
    return formData;
  }
  
  
export function _goPage(paramRouterName, paramQuery){
  // console.log("_goPage - 0 _goPage - 0 _goPage - 0")
  // console.log("paramRouterName", paramRouterName)
  // console.log("paramQuery", paramQuery)
    var query1 = ``;
    if (paramQuery== null || paramQuery === undefined || typeof paramQuery == "undefined") {
    } else {

      if (paramQuery.slug !== undefined) {
        Router.push({
          pathname: paramRouterName,
          query: removeByKey(paramQuery, 'slug')
        });
        return;
      }

      if (isEmptyObject(paramQuery)) {
        Router.push({
          pathname: paramRouterName,
        });
        return;
        
      } else {
        if (!paramRouterName) {
          return;
        }
        query1 = paramRouterName.split('?type=')
        Router.push({
          pathname: query1[0],
          query: paramQuery
        });

      }
    }
    if (paramRouterName &&paramRouterName.indexOf('?year=') > -1) {
      query1 = paramRouterName.split('?year=')
      Router.push({
        pathname: query1[0],
        query: { year : query1[1] }
      });
      return;
    }
    if (paramRouterName &&paramRouterName.indexOf('?year=') > -1) {
      query1 = paramRouterName.split('?year=')
      Router.push({
        pathname: query1[0],
        query: { year : query1[1] }
      });
      return;
    }
    if (paramRouterName &&paramRouterName.indexOf('?type=') > -1) {
      query1 = paramRouterName.split('?type=')
      Router.push({
        pathname: query1[0],
        query: { type : query1[1] }
      });
      return;
    }
    if (paramRouterName &&paramRouterName.indexOf('?type=') > -1) {
      query1 = paramRouterName.split('?type=')
      Router.push({
        pathname: query1[0],
        query: { type : query1[1] }
      });
      return;
    }
    if (paramQuery== null || paramQuery === undefined || typeof paramQuery == "undefined") {
      Router.push({ pathname: paramRouterName });
      return;
    }
    Router.push({
      pathname: paramRouterName,
      query: removeByKey(paramQuery, 'slug')
    },
    // undefined, 
    // { shallow: true }
    );
    return;
  };


export function isEmptyObject(param) {
  return Object.keys(param).length === 0 && param.constructor === Object;
}

export function removeByKey (myObj, deleteKey) {
  return Object.keys(myObj)
    .filter(key => key !== deleteKey)
    .reduce((result, current) => {
      result[current] = myObj[current];
      return result;
  }, {});
}



export function _refreshUserEssential() {

  if (typeof window === 'undefined') {
    // console.log("SSR")
    return;
  } else {

  }
}


export function _refreshToken() {

  if (typeof window === 'undefined') {
      // console.log("SSR")
      return;
  } else {

  }
  var stored_token = '';
  const access_token = localStorage.getItem('dasomtv__access__token')
  const is_auto_signin = localStorage.getItem('dasomtv__auto__sign')
  const sign_in_date = localStorage.getItem('dasomtv__sign__date')
  const access_token_s = sessionStorage.getItem('dasomtv__access__token')
  const is_auto_signin_s = sessionStorage.getItem('dasomtv__auto__sign')
  const sign_in_date_s = sessionStorage.getItem('dasomtv__sign__date')
  if (access_token_s == null || access_token_s === undefined || access_token_s == '') {
      sessionStorage.removeItem('dasomtv__access__token')
      sessionStorage.removeItem('dasomtv__auto__sign')
      sessionStorage.removeItem('dasomtv__sign__date')
      localStorage.removeItem('dasomtv__access__token')
      localStorage.removeItem('dasomtv__auto__sign')
      localStorage.removeItem('dasomtv__sign__date')
  }
  
  if (is_auto_signin == null || is_auto_signin === undefined || is_auto_signin == false ) {
      localStorage.removeItem('dasomtv__access__token')
      localStorage.removeItem('dasomtv__auto__sign')
      localStorage.removeItem('dasomtv__sign__date')
  } else {
      // if (sign_in_date) {
      //     var now = moment();
      //     var stored_date = moment(sign_in_date, 'YYYYMMDD')
      //     var duration = moment.duration(now.diff(stored_date));
      //     var days = duration.asDays();
      //     if (days > 30) {
      //         localStorage.removeItem('dasomtv__access__token')
      //         localStorage.removeItem('dasomtv__auto__sign')
      //         localStorage.removeItem('dasomtv__sign__date')
      //     }
      // } else {
      // }
  }
  if( access_token == null || access_token === undefined) {
  } else {
      if (access_token == '')  {
      } else {
          stored_token = access_token
      }
  }

  if( access_token_s == null || access_token_s === undefined) {
  } else {
      if (access_token_s == '')  {
      } else {
          stored_token = access_token_s
      }
  }
  
  return stored_token

}







export function _refreshCustomerCode() {
    if (typeof window === 'undefined') {
        // console.log("SSR")
        return '';
    } else {
  
    }
    var stored1 = '';
    const saved1 = localStorage.getItem('dasomtv__customerCode')
    if (saved1 == null || saved1 === undefined || saved1 == '') {
        stored1 = saved1;
    }
    return stored1
}
export function _refreshDeviceCode() {
    if (typeof window === 'undefined') {
        // console.log("SSR")
        return '';
    } else {
  
    }
    var stored1 = '';
    const saved1 = localStorage.getItem('dasomtv__deviceCode')
    if (saved1 == null || saved1 === undefined || saved1 == '') {
        stored1 = saved1;
    }
    return stored1
}

export function _refreshTel() {
    if (typeof window === 'undefined') {
        // console.log("SSR")
        return '';
    } else {
  
    }
    var stored1 = '';
    const saved1 = localStorage.getItem('dasomtv__tel')
    if (saved1 == null || saved1 === undefined || saved1 == '') {
        stored1 = saved1;
    }
    return stored1
}

export function _refreshPwd() {
    if (typeof window === 'undefined') {
        // console.log("SSR")
        return '';
    } else {
  
    }
    var stored1 = '';
    const saved1 = localStorage.getItem('dasomtv__pwd')
    if (saved1 == null || saved1 === undefined || saved1 == '') {
        stored1 = saved1;
    }
    return stored1
}

export function _refreshAutoLogin() {
    if (typeof window === 'undefined') {
        // console.log("SSR")
        return '';
    } else {
  
    }
    var stored1 = '';
    const saved1 = localStorage.getItem('dasomtv__autoLogin')
    if (saved1 == null || saved1 === undefined || saved1 == '') {
        stored1 = saved1;
    }
    return stored1
}

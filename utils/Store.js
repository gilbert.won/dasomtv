import React, { createContext, useReducer, useContext, } from 'react';
import Cookies from 'js-cookie';
import moment from 'moment';
import { useRouter } from 'next/router';
import { _goPage, _refreshToken, __apiGetMyInfo, 
    _refreshCustomerCode,
    _refreshDeviceCode,
    _refreshTel,
    _refreshPwd,
    _refreshAutoLogin,
} from './functions';
const asyncLocalStorage = {
  setItem: function (key, value) {
      return Promise.resolve().then(function () {
          localStorage.setItem(key, value);
      });
  },
  getItem: function (key) {
      return Promise.resolve(localStorage.getItem(key))
      // return Promise.resolve().then(function () {
      //     return localStorage.getItem(key);
      // });
  }
};

const asyncSessionStorage = {
  setItem: function (key, value) {
      return Promise.resolve().then(function () {
          sessionStorage.setItem(key, value);
      });
  },
  getItem: function (key) {
      return Promise.resolve(sessionStorage.getItem(key))
      // return Promise.resolve().then(function () {
      //     return sessionStorage.getItem(key);
      // });
  }
};

export const Store = createContext();
const initialState = {
  layoutMode: 'desktop',
  footerInfo :  Cookies.get('footerInfo')  ? JSON.parse(Cookies.get('footerInfo')) : [],
  languageMode : Cookies.get('languageMode') && Cookies.get('languageMode') === 'EN' ? 'EN' : 'KR',
  darkMode: Cookies.get('darkMode') === 'ON' ? true : false,
  page : {
    currentLoaded : '',
    previousLoaded : '/',
  },
  popup : {
    load : false,
    blur : false,
    code : '',
    object : null,
  },
  userEssential : {
    customerCode : Cookies.get('customerCode') ? Cookies.get('customerCode')  : "",
    deviceCode :  Cookies.get('deviceCode') ? Cookies.get('deviceCode')  : "",
    tel : Cookies.get('tel') ? Cookies.get('tel')  : "",
    pwd : Cookies.get('pwd') ? Cookies.get('pwd')  : "",
    autoLogin : Cookies.get('autoLogin') ? Cookies.get('autoLogin')  : "",
  },
  userInfo: Cookies.get('userInfo')
    ? JSON.parse(Cookies.get('userInfo'))
    : null,
  userToken : ''
};

// USER INFO
// connectedAvadin: Array(1)
//     // 0:
//     // name: "다솜모바일"
//     // role: "1" 
//     // serialNum: "Dev120220221gvaldt"
//     // sessionKey: "qqvpawliypnbyitepxlstahbdvhbfemujcwjfcejeqhfccyovzagcxphrkfgcilk"
// pwd: "ufifgpip"
// qb_id: 279100
// qb_token: "c877c0e1aa21282cfc18e4dff7d66a38a3000009"
// sessionKey: "cnhmxpipljwrcxjdboswnkeoiemltyuwnoctsonrhjzbwhzhymbvmoganeefdosm"
// status: "ok"
// status_code: 0
// token_status: "0"
// tvSerialNum: "Dev1Tv20220415beucsn"


function reducer(state, action) {
  switch (action.type) {
    case 'DARK_MODE_ON':
      return { ...state, darkMode: true };
    case 'DARK_MODE_OFF':
      return { ...state, darkMode: false };

    case 'FOOTER_ITEM_SET': {
      const arrayItems = action.payload;
      Cookies.set('footerInfo', JSON.stringify(arrayItems)) ;
      return { ...state, footerInfo: [...arrayItems] };
    }
    case 'USER_LOGIN':
      return { ...state, userInfo: action.payload };
    case 'USER_LOGOUT':

      sessionStorage.removeItem('dasomtv__access__token')
      sessionStorage.removeItem('dasomtv__auto__sign')
      sessionStorage.removeItem('dasomtv__sign__date')
      localStorage.removeItem('dasomtv__access__token')
      localStorage.removeItem('dasomtv__auto__sign')
      localStorage.removeItem('dasomtv__sign__date')

      localStorage.removeItem("dasomtv__customerCode");
      localStorage.removeItem("dasomtv__deviceCode");
      localStorage.removeItem("dasomtv__tel");
      localStorage.removeItem("dasomtv__pwd");
      localStorage.removeItem("dasomtv__autoLogin");

      return {
        ...state,
        userInfo: null,
        userToken : "",
      };
      

    default:
      return state;
  }
}


export function StoreProvider(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = { state, dispatch };
    const tokenStoredCheck = _refreshToken();

    const customerCode = _refreshCustomerCode();
    const deviceCode = _refreshDeviceCode();
    const tel = _refreshTel();
    const pwd = _refreshPwd();
    const autoLogin = _refreshAutoLogin();
    
    state.userEssential.customerCode = customerCode;
    state.userEssential.deviceCode = deviceCode;
    state.userEssential.tel = tel;
    state.userEssential.pwd = pwd;
    state.userEssential.autoLogin = autoLogin;

    console.log("local detect")
    console.log(customerCode)
    console.log("local detect")
    console.log(deviceCode)
    console.log("local tel")
    console.log(tel)
    console.log("local pwd")
    console.log(pwd)
    console.log("local autoLogin")
    console.log(autoLogin)

    state.userToken = tokenStoredCheck;
    if (tokenStoredCheck && tokenStoredCheck != "") {
    } else {
      state.userInfo = null;
      state.userToken = "";
    }
    const router = useRouter();
    if (state.page.currentLoaded != state.page.previousLoaded) {
      state.page.previousLoaded = state.page.currentLoaded 
    } else {

    }
    state.page.currentLoaded = router.pathname;

    return <Store.Provider value={value}>{props.children}</Store.Provider>;
}

# DasomTV
dasom tv service
vercel.com 이용 (테스트 중에는)

## Getting started
https://docs.google.com/presentation/d/1vue_RSyX4V9fYBylbIjo67zF93w1iQOb/edit#slide=id.p18
https://docs.google.com/spreadsheets/d/1McJ2RKW0lU-m7KmOz054t9KGAvZww4bG8sUZjqJnL44/edit#gid=0

=> 신규 정리 기획안 (19일)
https://docs.google.com/presentation/d/1EzAOwZtVOdc7a4AywxQdPtpPNMtGzi_4/edit#slide=id.p9

## API문서 / MQTT 등 규약 (우선은 solitary 만, 지역선택, 일반/홈 사용자 선택창 제외)
<!-- https://docs.google.com/spreadsheets/d/1FU0xKs6TxaVPNRZu350SimJGDF--WLdw/edit#gid=1697499694 -->
<!-- https://docs.google.com/presentation/d/1ph2MYZbyPCHU90YRPmnBjrfn944ZswFXtrBCSHy79VM/edit#slide=id.g11a5e50a0c8_0_0 -->

## TO BACKEND / SERVER 
Access to XMLHttpRequest at 'https://dev.dasomi.ai/API/Dev1/Dasom/user/avadin/tvLogin/' from origin 'http://localhost:3001' has been blocked by CORS policy: Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource.
- https://velog.io/@maliethy/nextjs-reverse-proxy%EB%A1%9C-cors%EC%97%90%EB%9F%AC-%ED%95%B4%EA%B2%B0%ED%95%98%EA%B8%B0
- https://www.youtube.com/watch?v=PNtFSVU-YTI
- https://dongmin-jang.medium.com/react-lets-solve-cors-in-nextjs-c3dae0b6dd79

// {
//   "tel": "MDEwMTExMTExMTM=",
//   "pwd": "MDAwMDAw3",
//   "autoLogin": "1"
// }
# 속도 최적화 관련
https://www.ciokorea.com/news/169744
https://velog.io/@yeonseo07/ReactRenderingSpeed
https://velog.io/@bluestragglr/쉽게-따라하는-프론트엔드-웹-어플리케이션-패키지-최적화
https://all-dev-kang.tistory.com/entry/Next-웹페이지의-성능-개선을-해보자-1-featlighthouse
https://helloinyong.tistory.com/316
https://wit.nts-corp.com/2020/12/28/6240
https://velog.io/@heojeongbo/Next.js-최적화
https://kimsangyeon-github-io.vercel.app/blog/2022-03-21-next-bundle-script
https://vroomfan.tistory.com/29
https://velog.io/@jt_include_rw/NextJS에서-글자가-깜빡일-때
https://minhyeong-jang.github.io/2019/12/12/nextjs-sc-twinkling
https://watermelonlike.tistory.com/180
https://vroomfan.tistory.com/22

# TV 개발환경 세팅 관련 (삼성 + LG까지)
https://ishappy.tistory.com/entry/삼성-스마트TV-앱-개발환경-설정하기
https://webostv.developer.lge.com
TV 화면 => 브라우저 접속 => 로딩부터 안됨. (수요일 이슈 파악예정)

# 웹 테스트 환경 


# 테스트 디바이스 (TV)
- SAMSUNG SERIF TV : 
- LG 스마트 TV : 


## 1 
"scripts": {
  "local": "NODE_ENV='development' node server.js",
  "dev": "next -p 3001",
  "build": "cross-env ANALYZE=true NODE_ENV=production next build",
  "start": "cross-env NODE_ENV=production next start -p 3001",
  "static-build": "next build && next export"
},

## 2

"scripts": {
  "local": "NODE_ENV='development' node server.js",
  "dev": "NODE_ENV='development' node server.js",
  "build": "cross-env ANALYZE=true NODE_ENV=production next build",
  "start": "cross-env NODE_ENV=production next start -p 3001",
  "static-build": "next build && next export"
},

# 3
"scripts": {
    "dev": "NODE_ENV=development node -r esm server.js",
    "build": "NODE_ENV=production next build",
    "start": "NODE_ENV=production node -r esm server.js",
},

## TEST
<video>
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4
 
<image>
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ElephantsDream.jpg
http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerBlazes.jpg
 
이 외에 더 많은 샘플 동영상이 필요할 경우

public text videos
gist.github.com/jsturgis/3b19447b304616f18657

화면 주사율
스크린 기기는 1초에 화면을 60번씩 그린다. (화면주사율이 60Hz이라는 뜻은 이걸 말한다.)
스크린 기기는 1초에 60번을 그리는데, 브라우저에서 이를 따라잡지 못하면 애니메이션이 자연스럽지 못하고 버벅거린다.
 
그렇다면 왜 1초에 60프레임이 안나오는 걸까?
이는 렌더링 과정과 연관이 있다. 이전 포스팅에서 말했듯, 브라우저가 화면을 그리는 것은 여러 단계로 나뉜다. 화면에 위치한 요소가 변경되면 요소의 사이즈와 간격 크기 등등을 다시 계산하여 화면에 그리는 레이아웃(리플로우)와 리페인트 과정이 일어난다.
애니메이션 과정 없이 변경한다면 1번만 다시 그리면 되지만, 만약 transition이나 animation을 이용해 부드러운 움직임을 주었다면, 브라우저는 1초에 60번씩 계산하고 다시 그리는 과정을 반복해야 한다.
많은 요소를 한꺼번에 초당 60번 이상 계산하여 화면에 그리게 되면 브라우저에 많은 부담이 가중되어 제대로 움직이지 못하고 버벅거리는 jank 현상이 일어나는 것이다.
 
출처: https://songsong.dev/7 



function preloading (imageArray) { let n = imageArray.length; for (let i = 0; i < n; i++) { let img = new Image(); img.src = imageArray[i]; } } 


preloading([ '1.png', '2.png', '3.png' ])

출처: https://mygumi.tistory.com/277 [마이구미의 HelloWorld]


재생 / 멈춤

$(function(){
  $('.play').click(function(){
  // [재생] 버튼을 클릭하면 -webkit-animation-play-state: running 속성 적용
    $('div').css('animation-play-state','running');
  });
  $('.stop').click(function(){
  // [멈춤] 버튼을 클릭하면 -webkit-animation-play-state: paused 속성 적용
    $('div').css('animation-play-state','paused');
  });
});



# MQTT LIBRARY 에러

https://www.emqx.com/en/blog/how-to-use-mqtt-in-react
https://github.com/eclipse/paho.mqtt.javascript
https://blog.actorsfit.com/a?ID=00001-916adc88-e485-4bc0-9cd6-43929b5a72c2
https://github.com/VictorHAS/mqtt-react-hooks
AWSIoTMQTTClient
https://levelup.gitconnected.com/mqtt-over-websocket-in-a-react-app-35ce96cd0844

import React from 'react';

import { Connector } from 'mqtt-react-hooks';
import Status from '../components/MQTT/Status';

export default function App() {
  return (
    <Connector brokerUrl="wss://test.mosquitto.org:1884">
      <Status />
    </Connector>
  );
}
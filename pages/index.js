import React, { useCallback, useEffect, useRef, useState, useContext } from 'react';
import Head from 'next/head';
import styled from 'styled-components';
// import { END } from 'redux-saga';

import GlobalFonts from '../public/fonts/fonts';
import Home from '../components/Home';
import PopupBlur1 from '../components/Popup/PopupBlur1';
import PopupFull1 from '../components/Popup/PopupFull1';
import PopupV1 from '../components/Popup/PopupV1';
import {decode as base64_decode, encode as base64_encode} from 'base-64';

import Cookies from 'js-cookie';
import { Store } from "../utils/Store";
import {ROOT_URL, AUTH_HEADER_STRING,  BASE_URL, LOGIN_POSTFIX,} from '../apis/index';
import * as Tv from '../apis/Tv';
import axios from 'axios';

const testId = "dasom";
const testPw = "1234";
const DasomTVHome = () => {

  const { state, dispatch } = useContext(Store);

  const [intStep, setIntStep] = useState(0);
  const [arrayTextInput, setArrayTextInput] = useState(["","","","","",""]);
  const [intFocusIndex, setIntFocusIndex] = useState(-1);
  const [textUserType, setTextUserType] = useState("");
  const [textCode, setTextCode] = useState("");
  const [textLocation, setTextLocation] = useState("");
  const [boolPopupOpen, setBoolPopupOpen] = useState(false);
  const [textAutoLogin, setTextAutoLogin] = useState("0");

  const [preloadImgPaths, setPreloadImgPaths] = useState([]);

  useEffect(()=> {
    // __apiCheck1();
    // __apiCheck2();
      var tempArr = [];
      // for (let i = 0; i < 407; i++) { 
      // let string1 = `/avatar1/avatar1_${i}.png`
      // tempArr.push(string1);
      // } 
      for (let i = 0; i < 168; i++) { 
      let string2 = `/avatar2/avatar2_${i}.png`
      tempArr.push(string2);
      } 
      setPreloadImgPaths([...tempArr])

      __apiPostNotification();
  }, [])

  function login() {
    __apiPostLogin();
    // setIntStep(101);
  }


  function __apiCheck1() {

    const req = {
      code : { 
        customerCode : "Dev1",
        deviceCode : "Dasom"
      },
      header: {
        // 'languageCode' : "ko-KR",
        'Content-Type' : "application/json" //;charset=UTF-8
      },
      data:  {
        tel: arrayTextInput[0],
        pwd: arrayTextInput[1],
        autoLogin: textAutoLogin,
      }
  };
    Tv.checkApi1(req)
    .then(res => {
        console.log("Tv.postLogin")
        console.log(res)
        // if (res.status == 208) {
        //     return;
        // }
        // if (res.status == 200) {
        //   return;
        // }
    });
  }
  function __apiCheck2() {

    const req = {
        code : { 
          customerCode : "Dev1",
          deviceCode : "Dasom"
        },
        header: {
          // 'languageCode' : "ko-KR",
          'Content-Type' : "application/json" //;charset=UTF-8
        },
        data:  {
          tel: "MDEwMTExMTExMTM=",
          pwd: "MDAwMDAw3",
          autoLogin: "1",
        }
    };
    axios.post( 
        // BASE_URL + `/${req.code.customerCode}/${req.code.deviceCode}` +  LOGIN_POSTFIX
        `/API/${req.code.customerCode}/${req.code.deviceCode}/user/avadin/tvLogin/`, 
      req.data,{
      headers:{
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': "*"
        // Authorization: 'Bearer ' + token // if you use token
      }
    })
    .then(res => {
      
    });
  }

  function __apiPostLogin() {

    console.log("__apiPostLogin - 0")
    console.log("arrayTextInput[0]", arrayTextInput[0])
    console.log("arrayTextInput[1]", arrayTextInput[1])
    let encoded1 = base64_encode(arrayTextInput[0]);
    let encoded2 = base64_encode(arrayTextInput[1]);

    console.log("__apiPostLogin - 1")
    const req = {
        code : { 
          customerCode : "Dev1",
          deviceCode : "Avadin"
        },
        header: {
          // 'languageCode' : "ko-KR",
          'Content-Type' : "application/json" //;charset=UTF-8
        },
        data:  {
          tel: encoded1,
          pwd: encoded2,
          autoLogin: textAutoLogin,
        }
    };
  axios.post( 
    // BASE_URL + `/${req.code.customerCode}/${req.code.deviceCode}` +  LOGIN_POSTFIX
    `/API/${req.code.customerCode}/${req.code.deviceCode}/user/avadin/tvLogin/`,
    req.data,
    {
      headers:{
        Accept: 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
        // Authorization: 'Bearer ' + token // if you use token
    }
    })
    .then(res => {
      console.log("---[api-login test] -- 001")
      console.log(res.data);

        // if (true) {

        //   Cookies.set("customerCode", req.code.customerCode);
        //   Cookies.set("deviceCode", req.code.deviceCode);
        //   Cookies.set("tel", req.data.tel);
        //   Cookies.set("pwd", req.data.pwd);
        //   Cookies.set("autoLogin", res.data.autoLogin);

        //   localStorage.setItem("dasomtv__customerCode",req.code.customerCode );
        //   localStorage.setItem("dasomtv__deviceCode",req.code.deviceCode );
        //   localStorage.setItem("dasomtv__tel",req.code.tel );
        //   localStorage.setItem("dasomtv__pwd",req.code.pwd );
        //   localStorage.setItem("dasomtv__autoLogin",req.code.autoLogin );
        //   state.userEssential.customerCode = req.code.customerCode;
        //   state.userEssential.deviceCode = req.code.deviceCode;
        //   state.userEssential.tel = req.data.tel;
        //   state.userEssential.pwd = req.data.pwd;
        //   state.userEssential.autoLogin = req.data.autoLogin;
        //   state.userInfo = res.data;
        //   // console.log(state.userInfo);
        //   setIntStep(101);
        //   return;
        // }


        if (res && res.data) {

          // 에러처리 (https://docs.google.com/spreadsheets/d/1FU0xKs6TxaVPNRZu350SimJGDF--WLdw/edit#gid=2051271660)
          if (res.data.status_code == 1001 || res.data.status_code == 1000 || res.data.status_code == -99) {
            alert(res.data.message);
            return;
          }
          // 성공처리 (처음유저와 처음이후유저의 response다름 : https://docs.google.com/spreadsheets/d/1FU0xKs6TxaVPNRZu350SimJGDF--WLdw/edit#gid=2051271660)
          if (res.data.status_code == 0) {

            Cookies.set("customerCode", req.code.customerCode);
            Cookies.set("deviceCode", req.code.deviceCode);
            Cookies.set("tel", req.data.tel);
            Cookies.set("pwd", req.data.pwd);
            Cookies.set("autoLogin", res.data.autoLogin);
  
            localStorage.setItem("dasomtv__customerCode",req.code.customerCode );
            localStorage.setItem("dasomtv__deviceCode",req.code.deviceCode );
            localStorage.setItem("dasomtv__tel",req.code.tel );
            localStorage.setItem("dasomtv__pwd",req.code.pwd );
            localStorage.setItem("dasomtv__autoLogin",req.code.autoLogin );
            state.userEssential.customerCode = req.code.customerCode;
            state.userEssential.deviceCode = req.code.deviceCode;
            state.userEssential.tel = req.data.tel;
            state.userEssential.pwd = req.data.pwd;
            state.userEssential.autoLogin = req.data.autoLogin;
            state.userInfo = res.data;
            // console.log(state.userInfo);
            setIntStep(101);
            return;
          }
          // ---------------------------- 로그인 성공 (최초 로그인 시)
          // {
          //   "status_code": 0,
          //   "status": "ok",
          //   "qb_id": "279107",
          //   "pwd": "nbbhjstq",
          //   "sessionKey": "gdbknpqxwdveuddyimsurllyierojnsnfpoufcktrlxpvyiwvhzlqxbrqsiaxfft", 
          //   "connectedAvadin" : ["serialNum"] - 현재 1개 리스트만 나옴, 추후 증가 예상에 따른 리스트 형태
          //   "tvSerialNum": "Dev1Tv20220415evhptt"
          // }
          // ---------------------------- 로그인 성공 (2회 이상 로그인 시) 
          // {
          //   "token_status": "0",
          //   "pwd": "ufifgpip",
          //   "qb_id": 279100,
          //   "qb_token": "a9e3355dc952f3939fdda781d931053b10000009",
          //   "status_code": 0,
          //   "status": "ok",
          //   "sessionKey": "vlllkwcoecriuczbyoyiwykjdeuyqxuocphnwnnpvydgbxaofzfcngtraxfpgsie",
          //   "connectedAvadin" : ["serialNum"] - 현재 1개 리스트만 나옴, 추후 증가 예상에 따른 리스트 형태
          //   "tvSerialNum": "Dev1Tv20220415beucsn"
          // }


        }
    });
}



function __apiPostNotification() {


  return;

  const req = {
      code : { 
        customerCode : "Dev1",
        deviceCode : "Avadin"
      },
      header: {
        // 'languageCode' : "ko-KR",
        'Content-Type' : "application/json" //;charset=UTF-8
      },
      data:  {
        tel: arrayTextInput[0],
        pwd: arrayTextInput[1],
        autoLogin: textAutoLogin,
      }
  };
  // {
  //   "tel": "MDEwMTExMTExMTM=",
  //   "pwd": "MDAwMDAw3",
  //   "autoLogin": "1"
  // }
  // https://docs.google.com/spreadsheets/d/1FU0xKs6TxaVPNRZu350SimJGDF--WLdw/edit#gid=1697499694
axios.post( 
  // BASE_URL + `/${req.code.customerCode}/${req.code.deviceCode}` +  LOGIN_POSTFIX
  `/API/${req.code.customerCode}/${req.code.deviceCode}/alarm/get_use_alarm/`,
  req.data,
  {
    headers:{
      Accept: 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': "*"
      // Authorization: 'Bearer ' + token // if you use token
  }
  })
  .then(res => {
  })
}
  // useEffect(()=> {
  //   setBoolPopupOpen(false);
  //   if (intStep == 101) {
  //     setTimeout(() => {
  //       setBoolPopupOpen(true);
  //     }, 1000); 
  //   }
  // }, [intStep]);

  return (
    <>
      <GlobalFonts />
      <Head>
        <title>다솜TV</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta property="og:title" content="Dasom TV" />
        <meta property="og:description" content="" />
        {/* <meta property="og:image" content="" /> */}
        <meta property="og:url" content="" />
      </Head>
      <PreloadImgContainer  style={{ display: 'none !important', zIndex: -99, width: 0, height: 0}}>
        {
          preloadImgPaths && preloadImgPaths.map((arrayItem, arrayIndex)=> {
              return (
              <div className='' style={{ display: 'none !important', zIndex: -99, width: 0, height: 0}}>
                  <img src={arrayItem}  style={{ display: 'none !important', zIndex: -99, width: 0, height: 0}}/>
              </div>
              )
          })
        }
      </PreloadImgContainer>

      {boolPopupOpen == true && (
          <PopupFull1
          style={{ zIndex: 91 }}
          onClick={e => {
            e.stopPropagation();
            e.preventDefault();
            setBoolPopupOpen(false)
          }}>
              <PopupBlur1
                  paramObject={null}
                  onClick={e => {
                    e.stopPropagation();
                    e.preventDefault();
                      setBoolPopupOpen(false)
                  }}
              ></PopupBlur1>
              <PopupV1 setBoolPopup1={()=> {
                      setBoolPopupOpen(false)
                  }}>
                <AlarmPopupInner>
                  <div className='div0 div1'>
                    <div className='alarmtext0 '>
                      원더풀님,<br/>
                      <span className='' style={{ fontWeight: '700'}}>프로바이오틱스</span><br/>
                      복용시간입니다.
                    </div>
                      <img className='image0 image1' src="/general/icon_medicine.png" alt="" />
                  </div>
                  <div className='div0 div2'>
                    <div className='timewrapper0 timewrapper1'>
                      <div className=' text1'>
                        10:30<div className=' text2'>am</div>
                      </div>
                    </div>
                    <div className='button0 button1'>
                      확인
                    </div>

                  </div>
                </AlarmPopupInner>
              </PopupV1>
          </PopupFull1>
      )}
      {
        intStep == 0 &&
          <StepContainer1>
          <div className='label0 label1'>시니어를 위한 TV서비스</div>
            <img className='image0 image1' src="/dasomtv/logo0.png" alt="" />
            <img className='image0 image2' src="/dasomtv/center0.png" alt="" />
            <img className='image0 image3'  src="/dasomtv/back0.png" alt="" />
            <div className='div1'
              onClick={()=> {
                setIntStep(100);
              }}
            >
              다솜TV 로그인 하기
            </div>
        </StepContainer1>
      }
      {
        intStep == 1 &&

      <StepContainer2>
      <img className='image0 image3'  src="/dasomtv/back0.png" alt="" />
        <div className='div0'>
          <img className='image0 image1' src="/dasomtv/logo0.png" alt="" />
        </div>
        <div className='div1'>
            <LabelText1>사용자 유형을 선택하세요.</LabelText1>
            {
              Array.from([
                {
                  type : "일반사용자",
                  code : "",
                  iconPath : 'general/icon_senior1.png'
                }, 
                {
                  type : "홈케어 사용자",
                  code : "",
                  iconPath : 'general/icon_heartedhome1.png'
                }
              ]).map((inputItem, inputIndex)=> {
                return (
                  <>
                  <SelectWrapper 
                    style={{ border: intFocusIndex == inputIndex ? `3px solid #fff` : `3px solid transparent`, }}
                    onClick={()=> {
                      setTextUserType("" + inputItem[inputIndex])
                      setIntStep(2)
                    }}
                  >
                    <img className='image0 image1' src={inputItem.iconPath} alt="" />
                    <div className='label0 label1'>{inputItem.type}</div>
                  </SelectWrapper>
                </>
                )
              })
            }
        </div>
      </StepContainer2>
      }
      {
        intStep == 2 &&
        <StepContainer2>
        <img className='image0 image3'  src="/dasomtv/back0.png" alt="" />

          <div className='div0'>
            <img className='image0 image1' src="/dasomtv/logo0.png" alt="" />
          </div>
          <div className='div1'>
              <LabelText1>
                {
                  `정부 또는 홈케어 업체로부터
                  발급받은 서비스 코드를 입력
                  해주세요.`
                }
              </LabelText1>
              {
                Array.from(["코드",]).map((inputItem, inputIndex)=> {
                  return (
                    <>
                    <LabelText2 >{inputItem}</LabelText2>
                    <TextInputWrapper style={{ border: intFocusIndex == inputIndex ? `3px solid #fff` : `3px solid transparent`}}>
                    <TextInput
                      type="text"
                      placeholder={inputItem + "를 입력해주세요."}
                      value={textCode}
                      onChange={(e) => {
                        setTextCode(e.target.value);
                      }}
                      onFocus={e => {
                          e.preventDefault();
                          e.stopPropagation();
                          setIntFocusIndex(inputIndex)
                      }}
                      // onKeyPress={(e) => {
                      //   if (window.event.keyCode === 13) {
                      //     login();
                      //   }
                      // }}
                    />
                  </TextInputWrapper>
                  </>
                  )
                })
              }
              {/* #4CA7FC */}
              <RoundButton 
                className='submitRow' 
                style={{ 
                  background : textCode != ""  ? `#4CA7FC` : `#59686B` ,
                  color: textCode != "" ? `#fff`: `rgba(255, 255, 255, 0.24)`
                }}
                onClick={()=> {
                  // login();
                  setIntStep(3)
                }}
              >
                코드확인
              </RoundButton>

          </div>
        </StepContainer2>
      }
      {
        intStep == 3 &&
        <StepContainer2>

        {boolPopupOpen == true && (
            <PopupFull1
            onClick={e => {
              e.stopPropagation();
              e.preventDefault();
              setBoolPopupOpen(false)
            }}>
                <PopupBlur1
                    paramObject={null}
                    onClick={e => {
                      e.stopPropagation();
                      e.preventDefault();
                        setBoolPopupOpen(false)
                    }}
                ></PopupBlur1>
                <PopupV1 setBoolPopup1={()=> {
                        setBoolPopupOpen(false)
                    }}>
                  <LocationPopupInner>
                    <div className='title0 title1'>이용 지역 선택</div>
                    <div className='hline'></div>
                    {
                      Array.from(["미국", "대한민국", "일본", "중국","캐나다", ]).map((locItem, locIndex)=> {
                        return (
                          <div 
                            className={'item0 item1'}
                            onClick={e=> {
                              e.preventDefault();
                              e.stopPropagation();
                              setTextLocation(locItem);
                              setBoolPopupOpen(false)

                            }}
                          >
                            {locItem}
                            {
                              textLocation == locItem &&
                              <>
                                <img className='selectedicon0' src="/general/icon_check0.png" alt="" />
                                <div className='selected0 selected1'>선택됨</div>
                              </>

                            }
                          </div>
                        )
                      })
                    }
                  </LocationPopupInner>
                </PopupV1>
            </PopupFull1>
        )}

        <img className='image0 image3'  src="/dasomtv/back0.png" alt="" />

          <div className='div0'>
            <img className='image0 image1' src="/dasomtv/logo0.png" alt="" />
          </div>
          <div className='div1'>
              <LabelText1>
                {
                  `다솜 서비스를 이용할 지역을
                  선택하세요.
                  `
                }
              </LabelText1>
              {
                Array.from([{
                  label : "지역 선택",
                  placeholder : "사용 지역을 선택해주세요.",
                  options: []
                }]).map((inputItem, inputIndex)=> {
                  return (
                    <>
                    <LabelText2 >{inputItem.label}</LabelText2>
                    <TextInputWrapper style={{ border: intFocusIndex == inputIndex ? `3px solid #fff` : `3px solid transparent`}}>
                    <TextInput
                      type="text"
                      placeholder={inputItem.placeholder}
                      value={textLocation}
                      onChange={(e) => {
                        setTextLocation(e.target.value);
                      }}
                      onFocus={e => {
                          e.preventDefault();
                          e.stopPropagation();
                          setIntFocusIndex(inputIndex)
                      }}
                      // onKeyPress={(e) => {
                      //   if (window.event.keyCode === 13) {
                      //     login();
                      //   }
                      // }}
                      onClick={e=> {
                        e.preventDefault();
                        e.stopPropagation();
                        setBoolPopupOpen(true);
                      }}
                    />
                  </TextInputWrapper>
                  </>
                  )
                })
              }
              <RoundButton 
                className='submitRow' 
                style={{ 
                  background : textLocation != "" ? `#4CA7FC` : `#59686B` ,
                  color: textLocation != ""  ? `#fff`: `rgba(255, 255, 255, 0.24)`
                }}
                onClick={()=> {
                  login();
                }}
              >
                다음
              </RoundButton>

          </div>
        </StepContainer2>
      }
      {
        intStep == 100 && 
      <StepContainer2>
      <img className='image0 image3'  src="/dasomtv/back0.png" alt="" />

        <div className='div0'>
          <img className='image0 image1' src="/dasomtv/logo0.png" alt="" />
        </div>
        <div className='div1'>
            <LabelText1>로그인</LabelText1>
            {
              Array.from(["아이디","비밀번호",]).map((inputItem, inputIndex)=> {
                return (
                  <>
                  <LabelText2 >{inputItem}</LabelText2>
                  <DasomTextInputWrapper style={{ border: intFocusIndex == inputIndex ? `3px solid #fff` : `3px solid transparent`}}>
                    <input
                      type={inputItem == "비밀번호"?  "password" : "text"}
                      placeholder={inputItem}
                    value={
                      arrayTextInput[inputIndex]
                    }
                    onChange={(e) => {
                      var inputs = [...arrayTextInput];
                      inputs[inputIndex] = e.target.value;
                      setArrayTextInput(inputs);
                    }}
                    onFocus={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        setIntFocusIndex(inputIndex)
                    }}
                    onKeyPress={(e) => {
                      if (window.event.keyCode === 13) {
                        login();
                      }
                    }}
                      />
                    </DasomTextInputWrapper>
                </>
                )
              })
            }
            <AutoLoginRow>
              <div className='checkBox0 '
              style={{ 
                background : textAutoLogin == "1" ? `#4CA7FC` : `#59686B` ,
                color: textAutoLogin == "1" ? `#fff`: `rgba(255, 255, 255, 0.24)`
              }}
              onClick={()=> {
                if (textAutoLogin == "0") {
                  setTextAutoLogin("1");
                } else {
                  setTextAutoLogin("0");
                }
              }}
              ></div>
              <div className='autoLabel0 label1'
              onClick={()=> {
                if (textAutoLogin == "0") {
                  setTextAutoLogin("1");
                } else {
                  setTextAutoLogin("0");
                }
              }}
              >자동로그인</div>
            </AutoLoginRow>
            {/* #4CA7FC */}
            <RoundButton 
              className='submitRow' 
              style={{ 
                background : arrayTextInput[0] != "" && arrayTextInput[1] != "" ? `#4CA7FC` : `#59686B` ,
                color: arrayTextInput[0] != "" && arrayTextInput[1] != "" ? `#fff`: `rgba(255, 255, 255, 0.24)`
              }}
              onClick={()=> {
                if (arrayTextInput[0] != "" && arrayTextInput[1] != "") {
                  login();
                  return;
                }
                alert('입력란을 확인해주세요.');
                return;
              }}
            >
              다솜TV 로그인 하기
            </RoundButton>
            
        </div>
      </StepContainer2>
      }

      {
        intStep > 100 && 
        <Home style={{ zIndex: 1 }}/>
      }

    </>
  );
};
// export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
//   console.log('### Home SSR ###');
//   // const { host } = context.req.headers;

//   context.store.dispatch(END);
//   console.log('### Home SSR end ###', context.locale);
//   await context.store.sagaTask.toPromise();
// });
export default DasomTVHome;


const StepContainer1 = styled.div`
  width: calc(100% - 40px);
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin-left: auto;
  margin-right: auto;

  position: relative;

  .image1 {
    width: 536px;
    max-width: calc(50vw - 40px);
    height: 90px;
    object-fit: contain;
    z-index: 1;
  }
  .image2 {
    width: 777px;
    max-width: calc(50vw - 40px);
    height: 586px;
    object-fit: contain;
    z-index: 1;
  }
  .image3 {
    width: 100vw;
    height: 100vh;

    position: absolute;
    z-index: 0;
    object-fit: cover;

    
  }

  .label1 {
    width: 500px;
    max-width: calc(50vw - 40px);
    height: 59px;
    font-size: 40px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.9;
    letter-spacing: normal;
    text-align: center;
    color: #fff;

    margin-left: auto;
    margin-right: auto;
    z-index: 1;
  }

  .div0 {

    width: 50%;
    min-width: 400px;
    max-width: calc(50vw - 40px);
    height: calc(100% - 40px);
    min-height: 400px;
    display: flex;
    align-items: center;
    justify-content: center;

    margin-left: auto;
    margin-right: auto;

    border-radius: 8px;
    border: 1px solid #000;

  }
  .div1 {

    width: 624px;
    height: 120px;
    font-size: 48px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    letter-spacing: normal;
    text-align: center;
    color: #fff;
    background-color : #4CA7FC;

    display: flex;
    align-items: center;
    justify-content: center;
    
    border-radius: 80px;
    z-index: 1;

    margin-top: 80px;

  }
`;

const LabelText1 = styled.div`
  width: 50%;
  font-size: 56px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;
  z-index: 1;

  margin-left: 0px;
  margin-right: auto;
  margin-bottom: 24px;

`;
const LabelText2 = styled.div`

  width: 100%;
  height: 59px;

  font-size: 40px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;
  z-index: 1;


  margin-left: 0px;
  margin-right: auto;
`;



const StepContainer2 = styled.div`
width: 100vw;
height: 100vh;
display: flex;
align-items: center;
justify-content: center;
position: relative;

margin-left: auto;
margin-right: auto;

.image1 {
  width: 536px;
  max-width: calc(50vw - 40px);
  height: 90px;
  object-fit: contain;
  z-index: 1;
}
.image3 {
  width: 100vw;
  height: 100vh;

  position: absolute;
  z-index: 0;
  object-fit: cover;

  
}
.div0 {

  width: 50%;
  min-width: 400px;
  height: calc(100% - 40px);
  min-height: 400px;
  display: flex;
  align-items: center;
  justify-content: center;

  margin-left: auto;
  margin-right: auto;

  border-right: 1px solid #000;
  
}
.div1 {

  width: 50%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin-left: auto;
  margin-right: auto;

  border-radius: 8px;

}
 .label0 {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 8px;

  width: 154px;
  height: 83px;
  margin: 0 716px 24px 224px;
  font-family: Pretendard;
  font-size: 56px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
}

.div1 .codeinput_container0 {
  width: 100px;
  min-width: 100px;
  height: 100px;
  min-height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 8px;

}

.div1 .codeinput_container0 .codeinput_wrapper0 {

  width: 40px;
  min-width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 8px;
  border: 1px solid #000;

  margin-left: auto;
  margin-right: 8px;
  margin-bottom: 8px;

}


@media only screen and (max-width: 400px) {
  /* ... */
  flex-direction: column;
}
@media only screen and (min-width: 400px) {
  /* ... */
  flex-direction: column;
}
@media only screen and (min-width: 600px) {
  /* ... */
  flex-direction: column;
}
@media only screen and (min-width: 768px) {
  /* ... */
  flex-direction: row;
}
@media only screen and (min-width: 992px) {
  /* ... */
  flex-direction: row;
}
@media only screen and (min-width: 1200px) {
  /* ... */
  flex-direction: row;

  .image1 {
    max-width: calc(50vw - 40px);
  }
  
}


`;
const DasomTextInputWrapper = styled.div`
width: 100%;
max-width: calc(50vw - 40px);
height: 120px;
background-color: rgba(255, 255, 255, 0.24);
z-index: 9;

border-radius: 16px;

flex-direction: row;
display: flex;

margin-bottom: 20px;
margin-left: 0px;
margin-right: auto;

input {
  width: calc(100% - 40px);
  height: 100%;

  background-color: transparent;
  color: #fff;
  font-size: 40px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  
  z-index: 10;

  margin-left: auto;
  margin-right: auto;
  border: none;

  outline: none;
  &::placeholder {
    color: rgba(255, 255, 255, 0.5);
    font-size: 40px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    letter-spacing: normal;
    text-align: left;
    user-select: none;
  }
}
`;

const SelectWrapper = styled.div`
  width: 100%;
  max-width: calc(50vw - 200px);
  height: 200px;
  margin: 24px 0 40px 224px;
  border-radius: 16px;
  background-color: rgba(255, 255, 255, 0.24);
  font-size: 40px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.8;
  letter-spacing: normal;
  text-align: left;

  flex-direction: row;
  display: flex;

  margin-bottom: 20px;
  z-index: 1;

  margin-left: 0px;
  margin-right: auto;

  :hover {
    background: #4CA7FC;
  }

  .image0 {
    width: 100px;
    max-width: 200px;
    height: calc(100% - 20px);
    margin-top: auto;
    margin-bottom: auto;
    margin-left: auto;
  }
  .label0 {
    min-width: 400px;

    height: calc(100% - 20px);
    margin-top: auto;
    margin-bottom: auto;
    margin-left: 20px;
    margin-right: auto;
    font-family: 'Noto Sans KR', sans-serif;

  }

`;

const TextInput = styled.input`

  border: none;
  background-color: transparent;
  box-sizing: border-box;
  letter-spacing: 0.5px;
  overflow: hidden;
  color: #fff;
  font-size: 14px;
  width: calc(100% - 40px);
  height: 100%;
  margin-left: auto;
  margin-right: auto;

  margin-left: 20px;
  
  font-size: 40px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  user-select: none;

  outline: none;
  &::placeholder {
    color: rgba(255, 255, 255, 0.5);
    font-size: 40px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    letter-spacing: normal;
    text-align: left;
    user-select: none;
  }
`;



const RoundButton = styled.div`
width: 100%;
max-width: calc(50vw - 40px);
height: 120px;

border: 1px solid #000;
border-radius: 12px;
min-height: 40px;
position: relative;
font-size: 14px;

margin-bottom: 20px;

font-size: 48px;
font-weight: bold;
font-stretch: normal;
font-style: normal;
letter-spacing: normal;
text-align: center;
color: rgba(255, 255, 255, 0.24);
background-color: #59686B;

display: flex;
align-items: center;
justify-content: center;

margin-left: 0px;
margin-right: auto;
margin-top: 32px;

`;

const LocationPopupInner = styled.div`

width: 796px;
height: 815px;
padding: 48px;
opacity: 0.9;
border-radius: 16px;
background-color: #47a5ff;


.title0 {

  width: 286px;
  height: 71px;
  font-size: 48px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.58;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  
  margin-left: auto;
  margin-right: auto;
}
.hline {

  width: calc(100% - 10px);
  margin-left: auto;
  margin-right: auto;

  height: 2px;
  opacity: 0.7;
  background : #5D99E7;
}
.item0 {

  width: calc(100% - 30px);
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  
  height: 100px;
  border-radius: 16px;
  color: #fff;
  // background-color: rgba(255, 255, 255, 0.24);

  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  
  display: flex;
  align-items: center;
  padding-left: 20px;

  border : 3px solid transparent;
  background transparent: 

}

.item0:hover {
  border : 3px solid #fff;
  background :rgba(255, 255, 255, 0.24);
}

.selectedicon0 {
  margin-left: auto;
  margin-right: 10px;
  width: 32px;
  height: 24px;
  object-fit: contain;
}
.selected0 {
  margin-right: 20px;

  font-size: 40px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal; 
  letter-spacing: normal;
  text-align: right;
  color: #fff;
}

`;

const AlarmPopupInner = styled.div`
width: 560px;
height: 600px;
opacity: 0.9;
border-radius: 16px;
background-color: #47a5ff;
overflow: hidden;
display: flex;
flex-direction: row;
flex-wrap: wrap;

margin-left: auto;
margin-right: auto;

.div0 {
  width: calc(100% - 24px);
  height: 300px;
}
.div1 {
  background : #47a5ff;
  display: flex;
  align-items: center;
  justify-content: center;
}
.div1 .alarmtext0 {
  font-size: 40px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;
  margin-left: auto;
}
.div1 img {
  width: 30%;
  height: calc(100% - 40px);
  object-fit: contain;
  margin-left: auto;
  margin-right: 20px;
}
.title0 {

  width: 286px;
  height: 71px;
  font-size: 48px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  
  margin-left: auto;
  margin-right: auto;

  display: block;
}
.div2 {
  width: 100%;
  background : #ffffff;
}
.div2 .timewrapper0  {
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 40px;
}
.div2 .timewrapper0 .text1 {

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  font-size: 80px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #333;

  margin-left: auto;
  margin-right: auto;

}
.div2 .timewrapper0 .text2 {
  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #333;

  margin-left: 16px;
}
.button0 {

  margin-top: auto;

  width: calc(100% - 40px);
  height: 112px;
  border-radius: 24px;
  background-color: #47a5ff;
  margin-left: auto;
  margin-right: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.6;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
}
`;


const AutoLoginRow = styled.div`
  width: calc(100% - 10px);
  z-index:2;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-top: 20px;
  .checkBox0 {
    width: 40px;
    height : 40px;
    // background-color: rgba(255, 255, 255, 0.24);
  
    border-radius: 8px;
    
    display: flex;
    align-items: center;
    border : 3px solid #fff;

    margin-left: 0px;
  
  }
  .checkBox0:hover {
    border : 3px solid #fff;
    background :rgba(255, 255, 255, 0.24);
  }
  .checkBox1 {

  }
  .checkBox2 {

  }
  .autoLabel0 {
    font-size: 40px;
    font-weight: 300;
    white-space: nowrap;


    display: flex;
    align-items: center;
    color : #fff;
    
    margin-left: 20px;
    margin-right: auto;
  }



`;

const PreloadImgContainer = styled.div`
display: none !important; zIndex: -99; width: 0; height: 0;
img {

display: none !important; zIndex: -99; width: 0; height: 0;
}
`;

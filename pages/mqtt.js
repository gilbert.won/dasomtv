import React, { useEffect, useState } from 'react';
import { Connector, useSubscription } from 'mqtt-react-hooks';

const Children = () => {
  const { message, connectionStatus } = useSubscription('Dev1Tv20220429psrcpu');
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    if (message) setMessages((msgs) => [...msgs, message]);
  }, [message]);

  return (
    <>
      <span>{connectionStatus}</span>
      <hr />
      <span>{JSON.stringify(messages)}</span>
    </>
  );
};

const PAGE = () => {
  return (
    <Connector
      brokerUrl="tcp://34.84.120.228:1884"
      parserMethod={msg => msg} // msg is Buffer
    >
      <Children />
    </Connector>
  );
};
export default PAGE;
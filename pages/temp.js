import React, { useState, useEffect, useRef, useContext } from "react";
import { useRouter } from "next/router";

const PageComponent = ({ props }) => {
    const router = useRouter();
    return (
        <>
            <div id="animated-background" className="image-container">
            </div>
            <style jsx="true" global="true" suppressHydrationWarning>{`
            html, body {
                background : #ff6060; // test
            }
            #animated-background {
                width: 50vw;
                height: 50vh;

                animation: bgfade 5s infinite;
                background-size: cover;
                background-position: center;
                background-color: transparent;
                position: relative;
            }
            #animated-background  {
                position: absolute;
                width: 16.4vw;
                bottom: 60px;
                z-index: 1;
            }
            @keyframes bgfade {
                0% {
                    background-image: url('/avatar1/avatar1_0.png');
                }
                1% {
                    background-image: url('/avatar1/avatar1_1.png')
                }
                2% {
                    background-image: url('/avatar1/avatar1_2.png')
                }
                3% {
                    background-image: url('/avatar1/avatar1_3.png')
                }
                4% {
                    background-image: url('/avatar1/avatar1_4.png')
                }
                5% {
                    background-image: url('/avatar1/avatar1_5.png');
                }
                6% {
                    background-image: url('/avatar1/avatar1_6.png')
                }
                7% {
                    background-image: url('/avatar1/avatar1_7.png')
                }
                8% {
                    background-image: url('/avatar1/avatar1_8.png')
                }
                9% {
                    background-image: url('/avatar1/avatar1_9.png')
                }
                10% {
                    background-image: url('/avatar1/avatar1_10.png');
                }
                11% {
                    background-image: url('/avatar1/avatar1_11.png')
                }
                12% {
                    background-image: url('/avatar1/avatar1_12.png')
                }
                13% {
                    background-image: url('/avatar1/avatar1_13.png')
                }
                14% {
                    background-image: url('/avatar1/avatar1_14.png')
                }
                15% {
                    background-image: url('/avatar1/avatar1_15.png');
                }
                16% {
                    background-image: url('/avatar1/avatar1_16.png')
                }
                17% {
                    background-image: url('/avatar1/avatar1_17.png')
                }
                18% {
                    background-image: url('/avatar1/avatar1_18.png')
                }
                19% {
                    background-image: url('/avatar1/avatar1_19.png')
                }
                20% {
                    background-image: url('/avatar1/avatar1_20.png');
                }
                21% {
                    background-image: url('/avatar1/avatar1_21.png')
                }
                22% {
                    background-image: url('/avatar1/avatar1_22.png')
                }
                23% {
                    background-image: url('/avatar1/avatar1_23.png')
                }
                24% {
                    background-image: url('/avatar1/avatar1_24.png')
                }
                25% {
                    background-image: url('/avatar1/avatar1_25.png');
                }
                26% {
                    background-image: url('/avatar1/avatar1_26.png')
                }
                27% {
                    background-image: url('/avatar1/avatar1_27.png')
                }
                28% {
                    background-image: url('/avatar1/avatar1_28.png')
                }
                29% {
                    background-image: url('/avatar1/avatar1_29.png')
                }

                30% {
                    background-image: url('/avatar1/avatar1_30.png');
                }
                31% {
                    background-image: url('/avatar1/avatar1_31.png')
                }
                32% {
                    background-image: url('/avatar1/avatar1_32.png')
                }
                33% {
                    background-image: url('/avatar1/avatar1_33.png')
                }
                34% {
                    background-image: url('/avatar1/avatar1_34.png')
                }
                35% {
                    background-image: url('/avatar1/avatar1_35.png');
                }
                36% {
                    background-image: url('/avatar1/avatar1_36.png')
                }
                37% {
                    background-image: url('/avatar1/avatar1_37.png')
                }
                38% {
                    background-image: url('/avatar1/avatar1_38.png')
                }
                39% {
                    background-image: url('/avatar1/avatar1_39.png')
                }
                40% {
                    background-image: url('/avatar1/avatar1_40.png');
                }
                41% {
                    background-image: url('/avatar1/avatar1_41.png')
                }
                42% {
                    background-image: url('/avatar1/avatar1_42.png')
                }
                43% {
                    background-image: url('/avatar1/avatar1_43.png')
                }
                44% {
                    background-image: url('/avatar1/avatar1_44.png')
                }
                45% {
                    background-image: url('/avatar1/avatar1_45.png');
                }
                46% {
                    background-image: url('/avatar1/avatar1_46.png')
                }
                47% {
                    background-image: url('/avatar1/avatar1_47.png')
                }
                48% {
                    background-image: url('/avatar1/avatar1_48.png')
                }
                49% {
                    background-image: url('/avatar1/avatar1_49.png')
                }

                50% {
                    background-image: url('/avatar1/avatar1_50.png');
                }
                51% {
                    background-image: url('/avatar1/avatar1_51.png')
                }
                52% {
                    background-image: url('/avatar1/avatar1_52.png')
                }
                53% {
                    background-image: url('/avatar1/avatar1_53.png')
                }
                54% {
                    background-image: url('/avatar1/avatar1_54.png')
                }
                55% {
                    background-image: url('/avatar1/avatar1_55.png');
                }
                56% {
                    background-image: url('/avatar1/avatar1_56.png')
                }
                57% {
                    background-image: url('/avatar1/avatar1_57.png')
                }
                58% {
                    background-image: url('/avatar1/avatar1_58.png')
                }
                59% {
                    background-image: url('/avatar1/avatar1_59.png')
                }
                60% {
                    background-image: url('/avatar1/avatar1_60.png');
                }
                61% {
                    background-image: url('/avatar1/avatar1_61.png')
                }
                62% {
                    background-image: url('/avatar1/avatar1_62.png')
                }
                63% {
                    background-image: url('/avatar1/avatar1_63.png')
                }
                64% {
                    background-image: url('/avatar1/avatar1_64.png')
                }
                65% {
                    background-image: url('/avatar1/avatar1_65.png');
                }
                66% {
                    background-image: url('/avatar1/avatar1_66.png')
                }
                67% {
                    background-image: url('/avatar1/avatar1_67.png')
                }
                68% {
                    background-image: url('/avatar1/avatar1_68.png')
                }
                69% {
                    background-image: url('/avatar1/avatar1_69.png')
                }
                70% {
                    background-image: url('/avatar1/avatar1_70.png');
                }
                71% {
                    background-image: url('/avatar1/avatar1_71.png')
                }
                72% {
                    background-image: url('/avatar1/avatar1_72.png')
                }
                73% {
                    background-image: url('/avatar1/avatar1_73.png')
                }
                74% {
                    background-image: url('/avatar1/avatar1_74.png')
                }
                75% {
                    background-image: url('/avatar1/avatar1_75.png');
                }
                76% {
                    background-image: url('/avatar1/avatar1_76.png')
                }
                77% {
                    background-image: url('/avatar1/avatar1_77.png')
                }
                78% {
                    background-image: url('/avatar1/avatar1_78.png')
                }
                79% {
                    background-image: url('/avatar1/avatar1_79.png')
                }
                80% {
                    background-image: url('/avatar1/avatar1_80.png');
                }
                81% {
                    background-image: url('/avatar1/avatar1_81.png')
                }
                82% {
                    background-image: url('/avatar1/avatar1_82.png')
                }
                83% {
                    background-image: url('/avatar1/avatar1_83.png')
                }
                84% {
                    background-image: url('/avatar1/avatar1_84.png')
                }
                85% {
                    background-image: url('/avatar1/avatar1_85.png');
                }
                86% {
                    background-image: url('/avatar1/avatar1_86.png')
                }
                87% {
                    background-image: url('/avatar1/avatar1_87.png')
                }
                88% {
                    background-image: url('/avatar1/avatar1_88.png')
                }
                89% {
                    background-image: url('/avatar1/avatar1_89.png')
                }

                90% {
                    background-image: url('/avatar1/avatar1_90.png');
                }
                91% {
                    background-image: url('/avatar1/avatar1_91.png')
                }
                92% {
                    background-image: url('/avatar1/avatar1_92.png')
                }
                93% {
                    background-image: url('/avatar1/avatar1_93.png')
                }
                94% {
                    background-image: url('/avatar1/avatar1_94.png')
                }
                95% {
                    background-image: url('/avatar1/avatar1_95.png');
                }
                96% {
                    background-image: url('/avatar1/avatar1_96.png')
                }
                97% {
                    background-image: url('/avatar1/avatar1_97.png')
                }
                98% {
                    background-image: url('/avatar1/avatar1_98.png')
                }
                99% {
                    background-image: url('/avatar1/avatar1_99.png')
                }
                100% {
                    background-image: url('/avatar1/avatar1_100.png');
                }
            }
            /****** add a background overlay *******/
            #animated-background:before {
                content: "";
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }
            `}</style>
        </>
    );
};

export default PageComponent;

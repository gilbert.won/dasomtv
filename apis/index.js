// TEST / DEV
export const ROOT_URL = "https://dev.dasomi.ai/API";
export const BASE_URL = "https://dev.dasomi.ai/API";
// REAL / USE
// export const ROOT_URL = "https://channel.dasomi.ai/API";
// export const BASE_URL = "https://channel.dasomi.ai/API/";

export const ROOT_WEB_URL = "https://dev.dasomi.ai/";
// http://dev.dasomi.ai/API/{customerCode}/{deviceCode}
// https://channel.dasomi.ai/API/{customerCode}/{deviceCode}                

export const LOGIN_POSTFIX = "/user/avadin/tvLogin/"
// 다솜-M 회원가입 완료 조건

// BODY
// {
//     "tel": "MDEwMTExMTExMTM=",
//     "pwd": "MDAwMDAw",
//     "autoLogin": "1"
//   }

// SWAGGER
// http://34.107.168.113/swagger-ui.html#!/user-controller/avadinTvLoginUsingPOST
//  RESPONSE
// ---------------------------- 로그인 성공 (최초 로그인 시)
// {
//   "status_code": 0,
//   "status": "ok",
//   "qb_id": "279107",
//   "pwd": "nbbhjstq",
//   "sessionKey": "gdbknpqxwdveuddyimsurllyierojnsnfpoufcktrlxpvyiwvhzlqxbrqsiaxfft", 
//   "connectedAvadin" : ["serialNum"] - 현재 1개 리스트만 나옴, 추후 증가 예상에 따른 리스트 형태
//   "tvSerialNum": "Dev1Tv20220415evhptt"
// }
// ---------------------------- 로그인 성공 (2회 이상 로그인 시) 
// {
//   "token_status": "0",
//   "pwd": "ufifgpip",
//   "qb_id": 279100,
//   "qb_token": "a9e3355dc952f3939fdda781d931053b10000009",
//   "status_code": 0,
//   "status": "ok",
//   "sessionKey": "vlllkwcoecriuczbyoyiwykjdeuyqxuocphnwnnpvydgbxaofzfcngtraxfpgsie",
//   "connectedAvadin" : ["serialNum"] - 현재 1개 리스트만 나옴, 추후 증가 예상에 따른 리스트 형태
//   "tvSerialNum": "Dev1Tv20220415beucsn"
// }
// ---------------------------- 존재하지 않는 유저
// {
//   "status_code": 1001,
//   "status": "Not Exist",
//   "message": "존재하지 않는 유저입니다."
// }
// ---------------------------- 아이디/비밀번호 실패
// {
//   "status_code": 1000,
//   "status": "Authorization Fail",
//   "message": "아이디 또는 비밀번호를 확인해 주세요."
// }
// ---------------------------- 필수 파라미터 누락
// {
//   "status_code": -99,
//   "status": "Not Exist Required Param",
//   "message": "전화번호가 없습니다."
// }


export const GET_WHOLE_ALARM = "/alarm/get_use_alarm/"
// 로그인 시 return 받은 connectedAvadin 리스트의 값을 serialNum으로 보낸다.
// * 추 후 다수 아바딘 등록 확장성에 따라 리스트로 출력  
// * 현재 리스트는 한대 등록밖에 안됨

// {
// "serialNum" : connectedAvadin's value
// }

// BODY
// {
//     "PUDDING_SERIALNUM": "solitary20220315gjdumf"
//   }

// SWAGGER
// http://34.107.168.113/swagger-ui.html#!/alarm-controller/get_use_alarmUsingPOST

//  RESPONSE
// {
//     "status_code": 0,
//     "status": "ok",
//     "useAllAlarmList": {
//       "calendar": [
//         {
//           "ELDERLY_NAME": "Gahyeong Kim",
//           "ALARM_USE_YN": "Y",
//           "ELDERLY_NO": "2022041800002",
//           "ALARM_DTL": "시장보기",
//           "ALARM_REPEAT_INTERVAL": "0",
//           "ALARM_REPEAT_START": "2022-04-18 16:55:00",
//           "ALARM_TIME": "16:55:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-01-05/2022-01-05_16_52_09_MtTrZZ.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-29/2021-10-29_09_48_53_PEWMqY.mp3",
//           "ALARM_NO": 4115,
//           "ALARM_CODE": "CALENDAR",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "REGISTRATION_DATE": "2022-04-18 16:40:45",
//           "ALARM_NAME": "일정알람"
//         }
//       ],
//       "sleep": [
//         {
//           "ALARM_DELETE_YN": "N",
//           "ALARM_USE_YN": "Y",
//           "ALARM_TIME": "10:10:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-28/2021-10-28_13_29_31_kecIyW.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_47_TqRuIS.mp3",
//           "ALARM_NO": 292,
//           "PUSH_YN": "N",
//           "ALARM_CODE": "SLEEP",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "ALARM_DATE": "01111111",
//           "REGISTRATION_DATE": 1650293905000,
//           "ALARM_NAME": "취침 알람"
//         }
//       ],
//       "meal": [
//         {
//           "ALARM_DELETE_YN": "N",
//           "ALARM_USE_YN": "Y",
//           "ALARM_TIME": "08:00:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-21/2021-10-21_15_31_07_sODSIN.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_49_iYziIO.mp3",
//           "PUSH_YN": "N",
//           "ALARM_CODE": "MEAL",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "ALARM_DATE": "01111111",
//           "MEAL_CODE": "BREAKFAST",
//           "ALARM_NAME": "식사 알람"
//         },
//         {
//           "ALARM_DELETE_YN": "N",
//           "ALARM_USE_YN": "Y",
//           "ALARM_TIME": "13:00:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-21/2021-10-21_15_31_08_QtZbRA.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_50_PAMEnb.mp3",
//           "PUSH_YN": "N",
//           "ALARM_CODE": "MEAL",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "ALARM_DATE": "01111111",
//           "MEAL_CODE": "LUNCH",
//           "ALARM_NAME": "식사 알람"
//         },
//         {
//           "ALARM_DELETE_YN": "N",
//           "ALARM_USE_YN": "Y",
//           "ALARM_TIME": "18:00:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_52_JMylaO.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_52_HOEqfK.mp3",
//           "PUSH_YN": "N",
//           "ALARM_CODE": "MEAL",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "ALARM_DATE": "01111111",
//           "MEAL_CODE": "DINNER",
//           "ALARM_NAME": "식사 알람"
//         }
//       ],
//       "daily": {
//         "DAILY_ALARM_USE": "Y",
//         "DAILY_ALARM": "09:00:00",
//         "DEMENTIA_YN": "N"
//       },
//       "medication": [
//         {
//           "MEDICATION_PERIOD": "false",
//           "ELDERLY_NAME": "Gahyeong Kim",
//           "ALARM_USE_YN": "Y",
//           "ELDERLY_NO": "2022041800002",
//           "ALARM_DTL": "비타민",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "MEDICATION_CNT": 0,
//           "ALARM_TIME": "16:50:00",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-28/2021-10-28_13_29_47_XfUFNI.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-28/2021-10-28_13_28_44_tasGNj.mp3",
//           "ALARM_NO": 4114,
//           "ALARM_CODE": "MEDICATION",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_DATE": "01111111",
//           "REGISTRATION_DATE": "2022-04-18 16:40:32",
//           "ALARM_NAME": "복약알람"
//         }
//       ],
//       "medicationSub": [
//         {
//           "MEDICATION_PERIOD": "false",
//           "ELDERLY_NAME": "Gahyeong Kim",
//           "ALARM_USE_YN": "Y",
//           "MEDICATION_TYPE": "영양제",
//           "ELDERLY_NO": "2022041800002",
//           "ALARM_TIME_LIST": [
//             "15:10:00",
//             "15:15:00",
//             "15:20:00"
//           ],
//           "ALARM_DTL": "new medi",
//           "ALARM_REPEAT_INTERVAL": "1",
//           "audioUrl": "http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-27/2021-10-27_13_32_20_xmhemS.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-03/2022-03-03_14_10_44_KgBoir.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-27/2021-10-27_12_59_44_ZGQThD.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-03/2022-03-03_14_10_44_KgBoir.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-18/2022-03-18_15_33_14_CZtYGR.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2021-10-20/2021-10-20_11_39_47_vOCSBs.mp3,http://107.167.189.37:81/tts/alarm/ko-KR/2022-03-03/2022-03-03_14_10_44_KgBoir.mp3",
//           "IMG_URL": [],
//           "ALARM_NO": "4161",
//           "ALARM_CODE": "MEDICATION_V3",
//           "PUDDING_SERIALNUM": "solitary20220315gjdumf",
//           "ALARM_DATE": "01111111",
//           "REGISTRATION_DATE": "2022-04-19 15:04:05",
//           "ALARM_NAME": "new medi"
//         }
//       ]
//     }
//   }

import axios from 'axios';
import {ROOT_URL, AUTH_HEADER_STRING,  BASE_URL} from './index';

// customerCode	일반사용자 / 홈케어	"일반사용자 - 국내 = solitary ,  해외 = overseas 홈케어사용자 - 사용자 직접 입력"
// deviceCode	deviceCode	Avadin

// 기능
// 유튜브 영상 요청
// 복약 알람
// 일정 알람
// 날씨 안내
// 일반 발화

// 사전 조건
// "다솜 TV모드 설정 완료 후 다솜-M 통하여 ""유튜브 틀어줘"" 음성 입력"
// "다솜 TV모드 설정 완료 후 다솜-M 통하여 ""오후 2시에 혈압약 알람 설정해줘"" 음성 입력"
// "다솜 TV모드 설정 완료 후 다솜-M 통하여 ""오후 2시에 알람 설정해줘"" 음성 입력"
// "다솜 TV모드 설정 완료 후 다솜-M 통하여 ""날씨 알려줘"" 음성 입력"
// "다솜 TV모드 설정 완료 후 다솜-M 통하여 일반대화 음성 입력"
const LOGIN_POSTFIX = "/user/avadin/tvLogin/"
const GET_WHOLE_ALARM = "/alarm/get_use_alarm/"

export function getProductAsType(req){
	return axios({ method: 'GET', url: BASE_URL + `product/item/type/${req.query}`, data: null, headers: req.header })
}

// MQTT 비고 - 1
// audioUrl - tts 음성 url 
// url - 유튜브 플레이 영상 url
// 연속재생 (url 배열) 

// MQTT Payload - 1
// {
//     "status_code": 0,
//     "json": {
//         "audioUrl": "http://107.167.189.37:81/tts/real/2021-12-01/2021-12-01_17_53_49_FoqXSZ.mp3",
//         "clientId": "1032020001000A06",
//         "hint_data": "요즘 인기 많은 노래 틀어 드릴게요",
//         "data": {
//             "actionName": "Youtube_play",
//             "is_scenario": "n",
//             "items": 3,
//             "serviceName": "Youtube",
//             "title": [
//                 "임영웅 아파요",
//                 "김호중 바람남",
//                 "이찬원 시절인연"
//             ],
//             "url": [
//                 "https://www.youtube.com/watch?v=RAbcyOVrjpk",
//                 "https://www.youtube.com/watch?v=s-4fP_R1hMw",
//                 "https://www.youtube.com/watch?v=nLxP_T9ZT9g"
//             ],
//             "yet": "false"
//         },
//         "appId": "dasom2",
//         "assistant": "영상***",
//         "domain": "asr",
//         "hint": "요즘 인기 많은 노래 틀어 드릴게요",
//         "query": "영상틀어줘",
//         "languageCode": null,
//         "serviceDomain": "music_play"
//     },
//     "status": "ok"
// }


// MQTT 비고 - 2
// "audioUrl - tts 음성 url 
// actionName - Alarm_medicine 의 경우 ""다솜 TV API"" 탭의 전체 알람 리스트 호출 후 알람 재정리"

// MQTT Payload - 2
// "{
//     ""status_code"": 0,
//     ""json"": {
//         ""clientId"": ""1032020001000A06"",
//         ""data"": {
//             ""actionName"": ""Alarm_medicine"",
//             ""date"": ""2022-04-20"",
//             ""is_scenario"": ""n"",
//             ""medicineName"": ""혈압약"",
//             ""period"": ""false"",
//             ""serviceName"": ""Alarm"",
//             ""time"": ""14:00:00"",
//             ""weekdays"": ""00010000"",
//             ""yet"": ""false""
//         },
//         ""assistant"": ""** 오후 *시* *** ** ****"",
//         ""dates"": [
//             ""2022-04-20""
//         ],
//         ""languageCode"": null,
//         ""serviceDomain"": ""alarm"",
//         ""status_data"": {
//             ""status_type"": ""success""
//         },
//         ""audioUrl"": ""http://107.167.189.37:81/tts/real/2022-04-19/2022-04-19_15_19_02_mYmhoF.mp3"",
//         ""hint_data"": ""4월 20일 오후 2시에 혈압약 복약 알람 설정했습니다."",
//         ""alarm_type"": 0,
//         ""alarm_save_data"": [
//             {
//                 ""actionName"": ""Alarm_medicine"",
//                 ""date"": ""2022-04-20"",
//                 ""medicineName"": ""혈압약"",
//                 ""period"": ""false"",
//                 ""serviceName"": ""Alarm"",
//                 ""time"": ""14:00:00"",
//                 ""weekdays"": ""00010000"",
//                 ""yet"": ""false""
//             }
//         ],
//         ""appId"": ""dasom"",
//         ""domain"": ""asr"",
//         ""hint"": ""4월 20일 오후 2시에 혈압약 복약 알람 설정했습니다.""
//     },
//     ""status"": ""ok""
// }"



// MQTT 비고 - 3
// "audioUrl - tts 음성 url 

// actionName - Alarm_infor 의 경우 
// 다솜 TV API 탭의 전체 알람 리스트 호출 후 알람 재정리"

// MQTT Payload - 3
// "{
//     ""status_code"": 0,
//     ""json"": {
//         ""clientId"": ""1032020001000A06"",
//         ""data"": {
//             ""actionName"": ""Alarm_infor"",
//             ""date"": ""2022-04-20"",
//             ""is_scenario"": ""n"",
//             ""period"": ""false"",
//             ""serviceName"": ""Alarm"",
//             ""time"": ""14:00:00"",
//             ""weekdays"": ""00000000"",
//             ""yet"": ""false""
//         },
//         ""assistant"": ""** 오후 *시* ** ****"",
//         ""dates"": [
//             ""2022-04-20""
//         ],
//         ""languageCode"": null,
//         ""serviceDomain"": ""alarm"",
//         ""status_data"": {
//             ""status_type"": ""success""
//         },
//         ""audioUrl"": ""http://107.167.189.37:81/tts/real/2022-04-19/2022-04-19_15_20_43_xbJrUL.mp3"",
//         ""hint_data"": ""4월 20일 오후 2시에 알람 설정했습니다."",
//         ""alarm_type"": 0,
//         ""alarm_save_data"": [
//             {
//                 ""actionName"": ""Alarm_infor"",
//                 ""date"": ""2022-04-20"",
//                 ""period"": ""false"",
//                 ""serviceName"": ""Alarm"",
//                 ""time"": ""14:00:00"",
//                 ""weekdays"": ""00000000"",
//                 ""yet"": ""false""
//             }
//         ],
//         ""appId"": ""dasom"",
//         ""domain"": ""asr"",
//         ""hint"": ""4월 20일 오후 2시에 알람 설정했습니다.""
//     },
//     ""status"": ""ok""
// }"

// MQTT 비고 - 4
// "audioUrl - tts 음성 url 
// actionName - Weather_infor"
// MQTT Payload - 4
// "{
//     ""status_code"": 0,
//     ""json"": {
//         ""clientId"": ""1032020001000A06"",
//         ""data"": {
//             ""actionName"": ""Weather_infor"",
//             ""curr_temp"": ""19.9"",
//             ""current_location"": ""양재1동"",
//             ""high_temp"": ""21.6"",
//             ""hint"": ""현재 서초구 양재1동의 날씨는 화창, 기온은 19.9도이며 오늘 최고기온 21.6도, 최저기온 7.3도 입니다. 미세먼지는 보통 입니다."",
//             ""is_scenario"": ""n"",
//             ""low_temp"": ""7.3"",
//             ""serviceName"": ""Weather"",
//             ""weather_code"": 1,
//             ""weather_status"": ""화창"",
//             ""yet"": ""false""
//         },
//         ""service_domain"": ""weather_info"",
//         ""assistant"": ""날씨 ***"",
//         ""languageCode"": null,
//         ""serviceDomain"": ""weather_info"",
//         ""result"": {
//             ""data"": {
//                 ""actionName"": ""Weather_infor"",
//                 ""curr_temp"": ""19.9"",
//                 ""current_location"": ""양재1동"",
//                 ""high_temp"": ""21.6"",
//                 ""hint"": ""현재 서초구 양재1동의 날씨는 화창, 기온은 19.9도이며 오늘 최고기온 21.6도, 최저기온 7.3도 입니다. 미세먼지는 보통 입니다."",
//                 ""low_temp"": ""7.3"",
//                 ""serviceName"": ""Weather"",
//                 ""weather_code"": 1,
//                 ""weather_status"": ""화창"",
//                 ""yet"": ""false""
//             }
//         },
//         ""audioUrl"": ""http://107.167.189.37:81/tts/real/2022-04-19/2022-04-19_16_30_25_xOONbl.mp3"",
//         ""hint_data"": ""현재 서초구 양재1동의 날씨는 화창, 기온은 19.9도이며 오늘 최고기온 21.6도, 최저기온 7.3도 입니다. 미세먼지는 보통 입니다."",
//         ""appId"": ""dasom2"",
//         ""domain"": ""asr"",
//         ""hint"": ""현재 서초구 양재1동의 날씨는 화창, 기온은 19.9도이며 오늘 최고기온 21.6도, 최저기온 7.3도 입니다. 미세먼지는 보통 입니다."",
//         ""device"": {
//             ""actionName"": ""Weather_infor"",
//             ""inbi_data"": {
//                 ""actionName"": ""Weather_infor"",
//                 ""curr_temp"": ""19.9"",
//                 ""current_location"": ""양재1동"",
//                 ""high_temp"": ""21.6"",
//                 ""hint"": ""현재 서초구 양재1동의 날씨는 화창, 기온은 19.9도이며 오늘 최고기온 21.6도, 최저기온 7.3도 입니다. 미세먼지는 보통 입니다."",
//                 ""low_temp"": ""7.3"",
//                 ""serviceName"": ""Weather"",
//                 ""weather_code"": 1,
//                 ""weather_status"": ""화창"",
//                 ""yet"": ""false""
//             },
//             ""serviceName"": ""Weather""
//         }
//     },
//     ""status"": ""ok""
// }"



// MQTT 비고 - 5
// "audioUrl - tts 음성 url 
// actionName - 위의 4개 actionName 이외의 값들은 일반발화로 처리"
// MQTT Payload - 5
// "{
//     ""status_code"": 0,
//     ""json"": {
//         ""clientId"": ""1032020001000A06"",
//         ""data"": {
//             ""actionName"": ""wonderful_chitchat"",
//             ""is_scenario"": ""n"",
//             ""serviceName"": ""emotok"",
//             ""yet"": ""false""
//         },
//         ""assistant"": ""***"",
//         ""en_hint"": ""It's nice to see you too."",
//         ""languageCode"": null,
//         ""serviceDomain"": ""emotok"",
//         ""malbut_classification_time"": 0.2074129581451416,
//         ""audioUrl"": ""http://107.167.189.37:81/tts/real/2021-12-05/2021-12-05_09_10_56_HMJZJf.mp3"",
//         ""hint_data"": ""저도 반가워요."",
//         ""appId"": ""dasom2"",
//         ""domain"": ""asr"",
//         ""hint"": ""저도 반가워요."",
//         ""model"": ""EMOTOK""
//     },
//     ""status"": ""ok""
// }"

import axios from 'axios';
import {ROOT_URL, AUTH_HEADER_STRING,  BASE_URL} from './index';

// customerCode	일반사용자 / 홈케어	(일반사용자 - 국내 = solitary ,해외 = overseas 홈케어사용자 - 사용자 직접 입력)
// deviceCode	Avadin
// /{customerCode}/{deviceCode}
const LOGIN_POSTFIX = "/user/avadin/tvLogin/"
const GET_WHOLE_ALARM = "/alarm/get_use_alarm/"

export function postLogin(req){
    //     customerCode, deviceCode
	return axios({ method: 'POST', url: BASE_URL + `/${req.code.customerCode}/${req.code.deviceCode}` +  LOGIN_POSTFIX, data: req.data, headers: req.header })
}

export function getWholeAlarm(req){
	return axios({ method: 'GET', url: BASE_URL + GET_WHOLE_ALARM, data: null, headers: req.header })
}


export function checkApi1(req){
	return axios({ method: 'GET', url:  BASE_URL + `/${req.code.customerCode}/${req.code.deviceCode}` +  LOGIN_POSTFIX, data: null, headers: req.header })
}
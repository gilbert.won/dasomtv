import React, { Component, useState, useEffect } from "react";
import styled from "styled-components";

const PopupV1 = styled.div`
    width: calc(100vw - 30px);
    max-width: 100%;
    position: absolute;
    top: 50%;
    transform: translate(0, -50%);

    z-index: 98;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;


    .close1 {
        position: absolute;
        right: 24px;
        top: 20px;
        width: 10px;
        height: 10px;
        object-fit: contain;
        cursor: pointer;
    }

    .label0 {
        display: inline-block;
        flex-direction: row;
        align-items: center;
        text-align: center;

        margin-left: auto;
        margin-right: auto;
    }

    .label1 {
        font-size: 20px;
        margin-top: 60px;
        margin-bottom: 48px;
    }

    .container0 {
        width: calc(100% - 100px);
        max-width: 900px;
        height: 800px;
        border-radius : 8px;
        color : #000;

        z-index: 99;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        
    }

    .container0 .text0 {
        width: 100%;
        height: calc(100% - 20px);
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .container0 p {
        display: flex;
        align-items: center;
        justify-content: center;
        width: calc(100% - 40px);
        padding: 20px;
        line-height: 1.7em;
    }

    .tablerow0 {
        display: flex;
        flex-direction : row;
        flex-wrap: nowrap;
        width: 100%;
        align-items: center;
        justify-content: center;
    }
    .tabledivider0 {
        display: flex;
        flex-direction : row;
        flex-wrap: nowrap;
        width: 100%;
        height: 1px;
        background-color: #4350AF;
        margin-top: 4px;
        margin-bottom: 4px;

    }
    .tableheader0 div  {
        width: calc(100% / 5);
        display: flex;
        align-items: center;
        justify-content: center;

    }
`;

const Comp = props => {
    const { children } = props;

    


    return (
        <PopupV1
            onClick={e => {
                e.stopPropagation();
                e.preventDefault();
            }}
        >
            <div className="container0 container1 border-trbl">
                <div
                    className="close1"
                    onClick={() => {
                        props.setBoolPopup1(false);
                    }}
                >
                    <span className="icon-Group-1795"></span>
                </div>
                <div className="text0 " 
        {...props}>{children}</div>
            </div>
        </PopupV1>
    );
};

export default Comp;

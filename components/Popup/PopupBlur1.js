import React, { Component, useState, useEffect } from 'react';
import styled from 'styled-components';
const PopupBlur1 = styled.div`
  width: 101vw;
  height: 101vh;
  opacity : 0.9;
  background-color : rgba(0,0,0,0.7);
  position: fixed;
  z-index: 91;
`;

const Comp = (props) => {
  const { children } = props;

  useEffect(() => {
  }, [])

  return (
    <PopupBlur1 
    {...props}
    >
      {children}
    </PopupBlur1>
  );
};

export default Comp;

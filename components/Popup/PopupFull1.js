import React, { Component, useState, useEffect } from "react";
import styled from "styled-components";

const PopupFull1 = styled.div`
    width: 101vw;
    height: 101vh;
    position: fixed;
    z-index: 91;

    left: 0;
    top: 0;
    

`;

const Comp = props => {
    const { children } = props;

    useEffect(() => {}, []);

    return (
        <PopupFull1
        {...props}>
            {children}
        </PopupFull1>
    );
};

export default Comp;

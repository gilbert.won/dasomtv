import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';

import { VideoTextBoxWrapper, VideoWrapper } from './styles';

const Video = ({ setIsViewOfVideo, youTubeCommand, youTubeUrl }) => {

  console.log('[dasomtv-debug] Video youTubeCommand :', youTubeCommand);
  console.log('[dasomtv-debug] Video youTubeUrl :', youTubeUrl);
  // [임시] 아바타 클릭 시 메인 화면으로 이동
  const handleClickAvatar = useCallback(() => {
    setIsViewOfVideo(false);
  }, []);


  // useEffect(()=> {
  //   var tempArr = [];
  //   for (let i = 0; i < 407; i++) { 
  //     let string1 = `/avatar1/avatar1_${i}.png`
  //     tempArr.push(string1);
  //   } 
  //   for (let i = 0; i < 168; i++) { 
  //     let string2 = `/avatar2/avatar2_${i}.png`
  //     tempArr.push(string2);
  //   } 
  //   console.log("preloading... -1")
  //   preloading([...tempArr])
  // }, [])

  // function preloading (imageArray) { 
  //   console.log("preloading... 0")
  //   let n = imageArray.length; 
  //   for (let i = 0; i < n; i++) { 
  //     let img = new Image(); 
  //     img.src = imageArray[i]; 
  //   }
  //  } 


  return (
    <VideoWrapper backgroundImage="url('/home/bg_video.png')">
      <div className="videoAvatarBox" onClick={handleClickAvatar}>
        {/* <img src="/home/main_avatar.png" alt="" /> */}
        <div id="animated-background2" className="image-container">
        </div>
      </div>
      <div className="iframeBox">
        <div>
          {/* [임시] 유튜브 영상 */}
          <ReactPlayer 
            url={youTubeUrl} 
            width="100%"
            height="100%"
            controls={false}
            playing
            autoPlay
            config={{
              file: {
                attributes: { preload: 'auto' },
              },
            }}
            onReady={() => console.log('onReady callback')}
            onStart={() => console.log('onStart callback')}
            onPause={() => console.log('onPause callback')}
            onError={() => console.log('onError callback')}
          
          />
        </div>
      </div>

      <VideoTextBoxWrapper>
        <div className="videoTextBox">
          <img src="/home/icon_info.png" alt="" />
          영상을 그만 보고 싶으신가요?
          <div><img src="/home/icon_mic.png" alt="" />다솜아</div>
          라고 하신 뒤
          <div><img src="/home/icon_mic.png" alt="" />정지해줘</div>
          라고 말씀해주세요.
        </div>
      </VideoTextBoxWrapper>
    </VideoWrapper>
  );
};

Video.propTypes = {
  setIsViewOfVideo: PropTypes.func.isRequired,
  youTubeCommand: PropTypes.string.isRequired,
  youTubeUrl: PropTypes.string.isRequired,
};

export default Video;
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';

import { VideoTextBoxWrapper, CallWrapper } from './styles';
import { useRouter } from "next/router";


const callObjectArray = [
    {
        label : `@###@님의 영상통화 요청`, 
        color0 : `#191e48`,
        color1 : `#0d324d`,
        color2 : `#004753`
    },
]

const Video = ({ setIsViewOfVideo, youTubeCommand, youTubeUrl }) => {

  const router = useRouter();
  
  const [intPageStep, setIntPageStep] = useState(0);

  console.log('[dasomtv-debug] Video youTubeCommand :', youTubeCommand);
  console.log('[dasomtv-debug] Video youTubeUrl :', youTubeUrl);
  // [임시] 아바타 클릭 시 메인 화면으로 이동
  const handleClickAvatar = useCallback(() => {
    setIsViewOfVideo(false);
  }, []);

  return (
    <CallWrapper backgroundImage="url('/home/bg_video.png')">
      <div className="videoAvatarBox" onClick={handleClickAvatar}>
        {/* <img src="/home/main_avatar.png" alt="" /> */}
        <div id="animated-background2" className="image-container">
        </div>
      </div>
      <div className="iframeBox">
        
        <div className='innerBox' style={{  background: `linear-gradient(to bottom, ${callObjectArray[0].color0} 0%, ${callObjectArray[0].color1} 50%, ${callObjectArray[0].color2} 100%)`}}>
            {
              intPageStep == 0 &&
              <>
              <div className='title0 title1'>
                  보호자3님의 영상통화 요청
              </div>
              <div className='column0 column1'>
                  <img src="/call/btn-reject.png" alt="" 
                    onClick={()=> {
                      router.replace("/")
                    }}
                  />
                  <div className='text0 text1'
                    onClick={()=> {
                      router.replace("/")
                    }}
                    >
                      거절
                  </div>
              </div>
              <div className='column0 column2'>
                  <img src="/call/icon_dot0.png" alt="" />
              </div>
              <div className='column0 column3'>
                  <img src="/call/profile-guardian.png" alt="" />
              </div>
              <div className='column0 column4'>
                  <img src="/call/icon_dot0.png" alt="" />
              </div>
              <div className='column0 column5'>
                  <img src="/call/btn-accept.png" alt="" 
                    onClick={()=> {
                      setIntPageStep(1);
                    }}
                  />
                  <div className='text0 text1'
                    onClick={()=> {
                      setIntPageStep(1);
                    }}
                  >
                      연결
                  </div>
              </div>
              </>
            }
            {
              intPageStep == 1 &&
              <>
              <div className='title0 title1'>
                  영상통화 발신중
                <br/>
                <div className='subtitle0 subtitle1'>
                    보호자2
                </div>
              </div>
              <div className='column0 column1'>
              </div>
              <div className='column0 column2'>
              </div>
              <div className='column0 column3'>
                  <img src="/call/profile-guardian.png" alt="" 
                    onClick={()=> {
                      setIntPageStep(2);
                    }}
                  />
              </div>
              <div className='column0 column4'>
                  <img src="/call/icon_dot0.png" alt="" />
              </div>
              <div className='column0 column5'>
                  <img src="/call/btn-reject.png" alt="" 
                    onClick={()=> {
                      router.replace("/")
                    }}
                  />
                  <div className='text0 text1'
                    onClick={()=> {
                      router.replace("/")
                    }}
                    >
                      취소
                  </div>
              </div>
              </>
            }
            {
              intPageStep == 2 &&
              <>
              <div className='title0 title1'>
                  통화중
                <br/>
                <div className='subtitle0 subtitle1'>
                    00:24
                </div>
              </div>
              <div className="fullScreen0 fullScreen1" >
                <img src="https://picsum.photos/200/300" />
              </div>
              <div className="smallScreen0 smallScreen1" >
                <img src="https://picsum.photos/200/300" />
              </div>
              <div className='bottombtn0 bottombtn1'>
                  <img src="/call/btn-reject.png" alt="" 
                    onClick={()=> {
                      router.replace("/")
                    }}
                  />
                  <div className='text0 text1'
                    onClick={()=> {
                      router.replace("/")
                    }}
                    >
                      취소
                  </div>
              </div>

              </>
            }
        </div>
      </div>

      <VideoTextBoxWrapper>
        <div className="videoTextBox">
          <img src="/home/icon_info.png" alt="" />
          전화 연결을 취소하려면
          <div><img src="/home/icon_mic.png" alt="" />다솜아</div>
          라고 하신 뒤
          <div><img src="/home/icon_mic.png" alt="" />취소해줘</div>
          라고 말씀해주세요.
        </div>
      </VideoTextBoxWrapper>
    </CallWrapper>
  );
};

Video.propTypes = {
  setIsViewOfVideo: PropTypes.func.isRequired,
  youTubeCommand: PropTypes.string.isRequired,
  youTubeUrl: PropTypes.string.isRequired,
};

export default Video;
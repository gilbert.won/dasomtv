import styled from 'styled-components';


// Video
export const CallWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: ${props => props.backgroundImage};
  background-size: 100% 100%;
  background-repeat: no-repeat;
  background-position: center;
  position: relative;

  // .videoAvatarBox {
  //   position: absolute;
  //   width: 16.4vw;
  //   bottom: 60px;
  //   z-index: 1;

  //   img {
  //     width: 100%;
  //   }
  // }
  .iframeBox {
    position: absolute;
    top: 8.5vh;
    width: 100vw;
    height: 69.1vh;
    text-align: center;
    z-index: 0;

    > div {
      width: 69vw;
      height: 69.1vh;
      border-radius: 16px;
      background: #000;
      margin: 0 auto;
    }
    iframe {
      width: 67.7vw;
      height: 68.5vh;
      padding-top: 0.15vw;
    }
  }

  .innerBox {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    position: relative;
}
.column0 {
    width: calc((100% - 200px) / 5);
    height: 100%;
    max-width: 300px;

    display: flex;
    flex-direction: column;
    align-items : center;

    position: relative;
    
}
.column3 {
    width: calc( 100% / 5);
}
.column1 {
    margin-left: auto;
}
.column2, 
.column3,
.column4 {

    margin-left: 20px;
    margin-right: 20px;
}
.column5 {
    margin-right: auto;
}
.column0 img {
    width: 100%;
    margin-top: auto;
    margin-bottom: auto;
}
.column1 img ,
.column5 img 
{
    max-width: 148px;
}
.column3 img 
{
    max-width: 327px;
}

.title0 {
    width: 100%;
    position: absolute;
    top: 80px;

    text-align: center;
    color : #fff;

    display: flex;
    align-items: center;
    justify-content: center;

  font-size: 48px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;

  flex-direction: column;
  z-index: 2;

}

.subtitle0 {


  font-size: 64px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  
  margin-top: 20px;
  
}

.column0 .text0 {
  text-align: center;
  color : #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 0px;
  position: absolute;
  top: calc(50% + 130px);

  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;

}
.fullScreen0 {
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 1;
}
.smallScreen0 {
  width: 200px;
  height: 300px;
  z-index: 2;
  position: absolute;
  bottom: 40px;
  left: 40px;
}
.fullScreen0 img ,
.smallScreen0 img {
  width: 100%;
  height: 100%;
  object-fit : cover;
}
.bottombtn0 {
  position: absolute;
  bottom: 40px;
  left: 50%;
  z-index: 2;
  transform: translate(-50%);
}
.bottombtn0 img 
{
    max-width: 148px;
}
.bottombtn0 .text0 {
  margin-top: 20px;
  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
}
}
`;
export const VideoTextBoxWrapper = styled.div`
  position: fixed;
  bottom: 60px;
  width: 100vw;
  z-index: 2;

  .videoTextBox {
    width: calc(100% - 244px);
    line-height: 65px;
    border-radius: 52px;
    background: rgba(255, 255, 255, 0.9);
    margin: 0 auto;
    padding: 19px 32px 20px;
    font-family: 'Pretendard';
    font-size: 44px;
    color: #000;
    letter-spacing: -2.2px;

    img {
      position: relative;
      top: -2px;
      margin-right: 16px;
      vertical-align: middle;
    }
    div {
      display: inline-block;
      height: 65px;
      line-height: 65px;
      border-radius: 36px;
      background: #47a5ff;
      margin: 0px 8px;
      padding: 0px 32px 0px 26px;
      color: #fff;
      vertical-align: middle;
    }
  }
`;
import styled from 'styled-components';



// Video
export const VideoWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: ${props => props.backgroundImage};
  background-size: 100% 100%;
  background-repeat: no-repeat;
  background-position: center;
  position: relative;

  // .videoAvatarBox {
  //   position: absolute;
  //   width: 16.4vw;
  //   bottom: 60px;
  //   z-index: 1;

  //   img {
  //     width: 100%;
  //   }
  // }
  .iframeBox {
    position: absolute;
    top: 8.5vh;
    width: 100vw;
    height: 69.1vh;
    text-align: center;
    z-index: 0;

    > div {
      width: 69vw;
      height: 69.1vh;
      border-radius: 16px;
      background: #000;
      margin: 0 auto;
    }
    iframe {
      width: 67.7vw;
      height: 68.5vh;
      padding-top: 0.15vw;
    }
  }


  .innerBox {
    display: flex;
    flex-direction: row;
}
.column0 {
  display: flex;
  flex-direction: column;
}
.column1 {
  width: 50%;
  height: 100%;
}
.column2 {
  width: 50%;
  height: 100%;
}
.text0 {
    color : #fff;
    display: flex;
    align-items: center;
}
.text0 img {

  width: 38px;
  height: 52px;
  margin: 0 24px 4px 0;
  object-fit: contain;
}
.column1 .text0 {
    width: calc(100% - 80px);
    margin-left: auto;
    margin-right: auto;
    white-space : nowrap;
}
.column1 .text1 {
  font-size: 48px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;

  margin-top: 40px;
}
.column1 .text2 {
    font-size: 265px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    letter-spacing: -13.25px;
    text-align: left;
    color: #fff;

    margin-top: 40px;
}
.column1 .text3 {

  font-size: 48px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: -2.4px;
  text-align: left;
  color: #fff;

  margin-top: 10px;
}
.column1 .text4 {

  font-size: 80px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;

  margin-top: 40px;
}
.column2 img {
    width: 100%;
    height: 100%;
    object-fit : contain;
}
`;
export const VideoTextBoxWrapper = styled.div`
  position: fixed;
  bottom: 60px;
  width: 100vw;
  z-index: 2;

  .videoTextBox {
    width: calc(100% - 244px);
    line-height: 65px;
    border-radius: 52px;
    background: rgba(255, 255, 255, 0.9);
    margin: 0 auto;
    padding: 19px 32px 20px;
    font-family: 'Pretendard';
    font-size: 44px;
    color: #000;
    letter-spacing: -2.2px;

    img {
      position: relative;
      top: -2px;
      margin-right: 16px;
      vertical-align: middle;
    }
    div {
      display: inline-block;
      height: 65px;
      line-height: 65px;
      border-radius: 36px;
      background: #47a5ff;
      margin: 0px 8px;
      padding: 0px 32px 0px 26px;
      color: #fff;
      vertical-align: middle;
    }
  }
`;
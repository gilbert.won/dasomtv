import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';

import { VideoTextBoxWrapper, VideoWrapper } from './styles';

const weatherObjectArray = [
    {
        label : "맑음", 
        color1 : `#4389cd`,
        color2 : `#4cabf5`,
        path : `/weather/icon_cloud_thunder.png`
    },
    { 
        label : "맑음", 
        color1 : "#232578", 
        color2 : "#5f619e" ,
        path : `/weather/icon_cloud_thunder.png`
    },
]

const Weather = ({ data,  }) => {
  console.log('[dasomtv-debug] Weather data :', data);

  const [hasNoti, setHasNoti] = useState(false);
  // [임시] 아바타 클릭 시 메인 화면으로 이동
  const handleClickAvatar = useCallback(() => {
    
  }, []);

  return (
    <VideoWrapper backgroundImage="url('/home/bg_video.png')">
      <div className="videoAvatarBox" onClick={handleClickAvatar}>
        {/* <img src="/home/main_avatar.png" alt="" /> */}
        <div id="animated-background2" className="image-container">
        </div>
      </div>
      <div className="iframeBox">
        <div className='innerBox' style={{  background: `linear-gradient(to bottom right, ${weatherObjectArray[0].color1} 0%, ${weatherObjectArray[0].color2} 100%)`}}>
            <div className='column0 column1'>
                <div className='text0 text1'>
                    {/* icon_pin_alt_fil */}
                    {/* icon_cloud_thunder.png 
                    icon_cloud_zap.png 
                    icon_cloud_zero.png 
                    icon_moon0.png 
                    icon_mooncloudmidrain.png 
                    icon_pin_alt_fil.png 
                    icon_senior1.png 
                    icon_sun0.png 
                    icon_suncloudrain.png 
                    icon_sunfastwind.png 
                    icon_tornado0.png  */}

                    <img src={"/weather/icon_pin_alt_fil.png"} alt="" />
                    {data && data.current_location}
                </div>
                <div className='text0 text2'>{data && data.curr_temp}<span>˚</span></div>
                <div className='text0 text3'>최고 {data && data.high_temp}˚ / 최저 {data && data.low_temp}˚</div>
                <div className='text0 text4'>{data && data.weather_status}</div>
            </div>
            <div className='column0 column2'>
                <img src={weatherObjectArray[0].path} alt="" />
            </div>
        </div>
      </div>

      <VideoTextBoxWrapper>
        <div className="videoTextBox">
          <img src="/home/icon_info.png" alt="" />
          오늘 서초구 양재동 날씨는 화창하고 맑은 상태가 하루종일 이어지겠습니다. 최고기온은 22도
        </div>
      </VideoTextBoxWrapper>
    </VideoWrapper>
  );
};

Weather.propTypes = {
  setIsViewOfVideo: PropTypes.func.isRequired,
  youTubeCommand: PropTypes.string.isRequired,
  youTubeUrl: PropTypes.string.isRequired,
};

export default Weather;
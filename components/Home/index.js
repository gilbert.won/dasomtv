import React, { useEffect, useState } from 'react';
import mqtt from 'mqtt';
import styled from 'styled-components';
import DasomTvMain from './Main';
import DasomTvVideo from './Video';
import DasomTvCall from './Call';
import DasomTvWeather from './Weather';
// import PopupBlur1 from '../popup/PopupBlur1';
// import PopupFull1 from '../Popup/PopupFull1';
// import PopupV1 from '../Popup/PopupV1';
import moment from 'moment';


const Container1 = styled.div`
  width: 100vw;
  height: 100vh;
  postion: relative;
  
`;

const Home = (props) => {
  // [임시] 홈 화면 <-> 유튜브 영상 화면 전환 위한 state
  const [isViewOfVideo, setIsViewOfVideo] = useState(false);

  const [actionName, setActionName] = useState('');
  const [audioUrl, setAudioUrl] = useState('');
  const [domain, setDomain] = useState('');
  const [hint, setHint] = useState('');
  const [youTubeUrl, setYoutubeUrl] = useState('');
  const [youtubeCommand, setYoutubeCommand] = useState('');
  // const [status, setStatus] = useState("");
  const [mqttMessage, setMqttMessage] = useState([]);
  const [mqttClient, setMqttClient] = useState(null);
  const [mqttPayload, setMqttPayload] = useState(null);

  const [boolTvFullScreen, setBoolTvFullScreen] = useState(false);
  const [boolAlaramPopupOpen, setBoolAlaramPopupOpen] = useState(false);
  const [objectAlaram, setObjectAlaram] = useState(null);
  const [objectInfo, setObjectInfo] = useState(null);

  console.log('[dasomtv-debug] Home isViewOfVideo : ', isViewOfVideo);
  console.log('[dasomtv-debug] Home actionName : ', actionName);
  console.log('[dasomtv-debug] Home audioUrl : ', audioUrl);
  console.log('[dasomtv-debug] Home domain : ', domain);
  console.log('[dasomtv-debug] Home hint : ', hint);
  console.log('[dasomtv-debug] Home youtubeUrl : ', youTubeUrl);

  // ////////////////////////////////////////////////////////////////////////////////////////
  // MQTT 시작
  // ////////////////////////////////////////////////////////////////////////////////////////

  // MQTT 연결
  const mqttConnect = (host, option) => {
    console.log('[dasomtv-debug] mqttDisconnect 연결중 !', new Date());
    console.log('[dasomtv-debug] mqttDisconnect option !', new Date());

    setMqttClient(mqtt.connect(host, option));
    console.log('[dasomtv-debug] mqttDisconnect mqtt.connect(host, option) : ', host);
  };

  // MQTT 연결 해제
  const mqttDisconnect = () => {
    if (mqttClient) {
      mqttClient.end(() => { 
        console.log('[dasomtv-debug] mqttDisconnect !', new Date());
      });
    }
  };

  // 렌더링 시 MQTT 연결 시도
  useEffect(() => {
    if (window.sessionStorage.getItem('emergencyCallList')) {
      window.sessionStorage.setItem('emergencyCallList', JSON.stringify([]));
    }

    console.log('[dasomtv-debug] useEffect 1 ! ');
    // 개발 서버용
    let host = '';
    let port = 0;
    let url = '';
    /*  if (window.location.protocol === 'http:') {
      // 개발(로컬)
      host = '34.84.120.228';
      port = 1883;
      url = `ws://${host}:${port}/mqtt`;
    } else {
      // 운영
      host = 'mqtt.dasomi.ai';
      port = 8883;
      url = `wss://${host}:${port}/mqtt`;
    } */

    host = 'mqtt.dasomi.ai';
    port = 8883;
    url = `wss://${host}:${port}/mqtt`;

    // id - sammy pw- 1thefull!
    const options = {
      id :'sammy',
      username: 'sammy',
      password: '1thefull!',
      clientId: `mqttjs_${Math.random().toString(16).substr(2, 8)}`, // 고정값
      protocolId: 'MQTT', // 고정값
      protocolVersion: 4, // 고정값
      clean: true,
      keepalive: 30,
      reconnectPeriod: 1000,  // 자동 reconnect 끄려면 0으로 설정
      connectTimeout: 30 * 1000,
      // will: {
      //   // topic: 'TempTopic',
      //   topic: 'monitor_data/web', 
      //   payload: 'Connection closed abnormally..!',
      //   qos: 0,
      //   retian: false,
      // },  // client 연결이 비정상적으로 해제됐을때
      rejectUnauthorized: false,
    };
    mqttConnect(url, options);

    // 종료 시 MQTT 연결 해제
    return () => { mqttDisconnect(); };
  }, []);

  // 특정 Topic 구독
  const mqttSubscribe = (subscription) => {
    if (mqttClient) {
      const { topic, qos } = subscription;
      mqttClient.subscribe(topic, { qos }, (error) => {
        if (error) { 
          // console.log('# 토픽 구독 오류', error); 
        }
      });
    }
  };

  // MQTT 연결 시 콜백함수 처리 (연결, 에러, 재연결, 메시지 수신)
  useEffect(() => {
    if (mqttClient) {
      console.log('[dasomtv-debug] mqttClient 연결완료!', mqttClient);
      // mqttSubscribe({ topic: 'TempTopic', qos: 0 });
        mqttClient.on('connect', () => { mqttSubscribe({ 
          topic: 'Dev1Tv20220429psrcpu', 
          qos: 0 
        }); 
      });
      // dasom_tv_proto
      mqttClient.on('error', (err) => { console.error('[dasomtv-debug] mqttClient 연결오류: ', err); mqttClient.end(); });
      mqttClient.on('reconnect', () => { /* console.log('[dasomtv-debug] mqttClient 재연결중..'); */ });
      mqttClient.on('message', async (topic, message) => {
        const payload = { topic, message: message.toString() };
        await setMqttPayload(payload);
      });
    }
  }, [mqttClient]);

  // 브로커에서 publish한 message 세팅
  useEffect(() => {
    if (mqttPayload && mqttPayload.topic) {
      // 아바딘아이오 console.log('# 구독 메시지\n', mqttPayload.message);
     
      // 다솜 TV
      // console.log('[dasomtv-debug] 구독 메시지 :', mqttPayload.message);
      const tempPayload = JSON.parse(mqttPayload.message);

      setMqttMessage(messages => {
        console.log('[dasomtv-debug] moment == ' , moment().format("YYYY-MM-DD HH:mm:ss"));
        console.log('[dasomtv-debug] tempPayload.json.data : ',  tempPayload.json.data);
        console.log('[dasomtv-debug] tempPayload.json.data.actionName : ', tempPayload.json.data.actionName);
        console.log('[dasomtv-debug] tempPayload.json.audioUrl : ', tempPayload.json.audioUrl);
        console.log('[dasomtv-debug] tempPayload.json.domain : ', tempPayload.json.domain);
        console.log('[dasomtv-debug] tempPayload.json.hint : ', tempPayload.json.hint);

        const tempActionName = tempPayload.json.data.actionName;
        if ( tempActionName == "Alarm_medicine" || tempActionName == "Alarm_infor" ) {
          setBoolAlaramPopupOpen(true);
          setObjectAlaram(tempPayload.json.data);

          let timer = setTimeout(()=>{ 
            setBoolAlaramPopupOpen(false);
            setObjectAlaram(null);
          }, 5000);

        } else {
          if ( tempActionName == "Weather_infor") {
            setObjectInfo(tempPayload.json.data)
          }

          setActionName(tempActionName);
          setObjectAlaram(null);
        }
        const tempAudioUrl = 
          tempPayload.json.audioUrl && tempPayload.json.audioUrl.replace('https://107.167.189.37:81/tts/', '/tts/').replace('http://107.167.189.37:81/tts/', '/tts/')
          ;

        console.log("tempAudioUrl", tempAudioUrl);
        setAudioUrl(tempAudioUrl);
        const tempDomain = tempPayload.json.domain;
        setDomain(tempDomain);
        let tempYoutubeUrl = '';
        const tempHint = tempPayload.json.hint;
        setHint(tempHint);
        
        if (tempActionName === 'Youtube_play') {
          for (let i = 0; i < tempPayload.json.data.url.length; i++) {
            if (i === 0) { 
              tempYoutubeUrl = tempPayload.json.data.url[i];
              console.log('[dasomtv-debug] tempYoutubeUrl : ', tempYoutubeUrl);
              setYoutubeUrl(tempYoutubeUrl);

              setIsViewOfVideo(true);
              setYoutubeCommand('1');

              // setTimeout(()=> {
              //   console.log("시간초 경과")
              //   setBoolTvFullScreen(true);
              // }, 10000);
            }
          }
        } else if (tempDomain === 'asr') {
          console.log('[dasomtv-debug] Action name is  not Youtube_play : ', tempActionName);
          console.log('[dasomtv-debug] Action name is  not Youtube_play , tempHint : ', tempHint);
            
          setIsViewOfVideo(false);
          if (tempHint === ',,,') {
            setHint('');
          } else { 
            setHint(tempHint);
          }
          setAudioUrl(tempAudioUrl);
        } 
        return messages;
      });
    }
  }, [mqttPayload]);


  // 응급콜 얼럿창 띄우기
  useEffect(() => {
    // const listInStorage = window.sessionStorage.getItem('emergencyCallList');
    // const list = listInStorage ? JSON.parse(listInStorage) : [];

    // MQTT로 publish 된 message 받은 후 (처음 들어온 메시지부터 처리)
    if (mqttMessage.length > 0) {
      // sessionStorage에 mqttMessage 추가
      /* 
      if (!listInStorage) {
        mqttMessage.forEach((each) => {
          list.push(JSON.parse(each.message));
        });
      } else {
        mqttMessage.forEach((each) => {
          if (!list.find((v) => v.sosIdx === JSON.parse(each.message).sosIdx)) {
            list.push(JSON.parse(each.message));
          }
        });
      }
      */
    }
  }, [mqttMessage]);


  // ////////////////////////////////////////////////////////////////////////////////////////
  // MQTT 종료
  // ////////////////////////////////////////////////////////////////////////////////////////

  /*
  useEffect(() => {
    if (youtubeUrl) {
      console.log('[dasomtv-debug] Home useEffect youtubeUrl : ', youtubeUrl);
    }
    if (actionName) {
      console.log('[dasomtv-debug] Home useEffect actionName : ', actionName);
    }
    if (audioUrl) {
      console.log('[dasomtv-debug] Home useEffect audioUrl : ', audioUrl);
    }
  }, [youtubeUrl, actionName, audioUrl]);
  */


  return (
    <Container1>
      {boolAlaramPopupOpen == true && (
        <PopupFull1
        style={{ zIndex: 91 }}
        onClick={e => {
          e.stopPropagation();
          e.preventDefault();
          setBoolAlaramPopupOpen(false);
        }}>
            <PopupBlur1
                paramObject={null}
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  setBoolAlaramPopupOpen(false);
                }}
            ></PopupBlur1>
            <PopupV1 setBoolPopup1={()=> {
                    setBoolAlaramPopupOpen(false);
            }}
            >
            {
              objectAlaram != null && 
              <AlarmPopupInner>
                <div className='div0 div1'>
                  <div className='alarmtext0 '>
                    원더풀님,<br/>
                    <span className='' style={{ fontWeight: '700'}}>  
                    {objectAlaram.actionName && objectAlaram.actionName == "Alarm_medicine" && objectAlaram.medicineName}
                    </span><br/>
                    {objectAlaram.actionName && objectAlaram.actionName == "Alarm_medicine" && "복약"}
                    {objectAlaram.actionName && objectAlaram.actionName == "Alarm_infor" && "알람 "}
                    시간입니다.
                  </div>
                  {objectAlaram.actionName && objectAlaram.actionName == "Alarm_medicine" && <img className='image0 image1' src="/general/icon_medicine.png" alt="" />}
                  {objectAlaram.actionName && objectAlaram.actionName == "Alarm_infor" && <img className='image0 image1' src="/general/icon_bell.png" alt="" />}
                </div>
                <div className='div0 div2'>
                  <div className='timewrapper0 timewrapper1'>
                    <div className=' text1'>
                      { objectAlaram.time}
                       {/* <div className=' text2'>am</div>  */}
                    </div>
                  </div>
                  <div className='button0 button1' onClick={()=> {
                    setBoolAlaramPopupOpen(false);
                  }}>
                    확인
                  </div>

                </div>
              </AlarmPopupInner>
            }
            </PopupV1>
        </PopupFull1>
      )} 

        {

          actionName == "Youtube_play" &&
          <DasomTvVideo 
          setBoolTvFullScreen={setBoolTvFullScreen} 
          setIsViewOfVideo={setIsViewOfVideo} 
          setActionName={setActionName} 
          setAudioUrl={setAudioUrl} 
          setDomain={setDomain} 
          setHint={setHint} 
          setYoutubeUrl={setYoutubeUrl} 
          youTubeUrl={youTubeUrl} 
          youTubeCommand={youtubeCommand} 
          boolTvFullScreen={boolTvFullScreen}
          />
        }
        {
          // Youtube_play, Alarm_medicine, Alarm_infor, Weather_infor, wonderful_chitchat
          actionName != "Youtube_play" && 
          <>
          
          {
            actionName == "Weather_infor" && 
            <DasomTvWeather 
            hint={hint} 
            audioUrl={audioUrl}  
            data={objectInfo}
            ></DasomTvWeather>
          }
          {/* 전화기능 mqtt규약 아직 없음 */}
          {/* {
  
            actionName == "Call_" && 
            <DasomTvCall 
            hint={hint} 
            audioUrl={audioUrl}  
            ></DasomTvCall>
          } */}
          {
            actionName != "Weather_infor" && 
            actionName != "Youtube_play" && 
            (
            actionName == "wonderful_chitchat" ||
            actionName == "Alarm_speech" ||
            actionName == "wikipediaMRC" || 
            actionName == "Service_suspend" || 
            actionName == "system_close_avadin_mode" ||
            actionName == "" 
            )
            &&
            <DasomTvMain 
            setIsViewOfVideo={setIsViewOfVideo} 
            hint={hint} 
            audioUrl={audioUrl}  
            />
          }
          </>
        }

    </Container1>
  );
};
export default Home;

const PopupBlur1 = styled.div`
  width: 101vw;
  height: 101vh;
  opacity : 0.9;
  background-color : rgba(0,0,0,0.7);
  position: fixed;
  z-index: 91;
`;

const PopupFull1 = styled.div`
    width: 101vw;
    height: 101vh;
    position: fixed;
    z-index: 91;

    left: 0;
    top: 0;
    

`;

const LocationPopupInner = styled.div`

width: 796px;
height: 815px;
padding: 48px;
opacity: 0.9;
border-radius: 16px;
background-color: #47a5ff;


.title0 {

  width: 286px;
  height: 71px;
  font-size: 48px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.58;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  
  margin-left: auto;
  margin-right: auto;
}
.hline {

  width: calc(100% - 10px);
  margin-left: auto;
  margin-right: auto;

  height: 2px;
  opacity: 0.7;
  background : #5D99E7;
}
.item0 {

  width: calc(100% - 30px);
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  
  height: 100px;
  border-radius: 16px;
  color: #fff;
  // background-color: rgba(255, 255, 255, 0.24);

  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  
  display: flex;
  align-items: center;
  padding-left: 20px;

  border : 3px solid transparent;
  background transparent: 

}

.item0:hover {
  border : 3px solid #fff;
  background :rgba(255, 255, 255, 0.24);
}

.selectedicon0 {
  margin-left: auto;
  margin-right: 10px;
  width: 32px;
  height: 24px;
  object-fit: contain;
}
.selected0 {
  margin-right: 20px;

  font-size: 40px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal; 
  letter-spacing: normal;
  text-align: right;
  color: #fff;
}

`;

const AlarmPopupInner = styled.div`
width: 560px;
height: 600px;
opacity: 0.9;
border-radius: 16px;
background-color: #47a5ff;
overflow: hidden;
display: flex;
flex-direction: row;
flex-wrap: wrap;

margin-left: auto;
margin-right: auto;

.div0 {
  width: calc(100% - 24px);
  height: 300px;
}
.div1 {
  background : #47a5ff;
  display: flex;
  align-items: center;
  justify-content: center;
}
.div1 .alarmtext0 {
  font-size: 40px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;
  margin-left: auto;
}
.div1 img {
  width: 30%;
  height: calc(100% - 40px);
  object-fit: contain;
  margin-left: auto;
  margin-right: 20px;
}
.title0 {

  width: 286px;
  height: 71px;
  font-size: 48px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
  
  margin-left: auto;
  margin-right: auto;

  display: block;
}
.div2 {
  width: 100%;
  background : #ffffff;
}
.div2 .timewrapper0  {
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 40px;
}
.div2 .timewrapper0 .text1 {

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  font-size: 80px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #333;

  margin-left: auto;
  margin-right: auto;

}
.div2 .timewrapper0 .text2 {
  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #333;

  margin-left: 16px;
}
.button0 {

  margin-top: auto;

  width: calc(100% - 40px);
  height: 112px;
  border-radius: 24px;
  background-color: #47a5ff;
  margin-left: auto;
  margin-right: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  font-size: 40px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.6;
  letter-spacing: normal;
  text-align: center;
  color: #fff;
}
`;

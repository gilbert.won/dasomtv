import React, { useCallback, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { DasomNotiWrapper, MainWrapper } from './styles';

const Main = ({ setIsViewOfVideo, hint, audioUrl }) => {
  // [임시] 다솜(아바타)의 대화 데이터 유무 판단 state

  const [hasNoti, setHasNoti] = useState(false);
  const avatarRef = useRef(null);

  let tempTtsUrl = '';


//   useEffect(()=> {
//     var tempArr = [];
//     for (let i = 0; i < 407; i++) { 
//       let string1 = `/avatar2/avatar2_${i}.png`
//       tempArr.push(string1);
//     } 
//     for (let i = 0; i < 168; i++) { 
//       let string2 = `/avatar2/avatar2_${i}.png`
//       tempArr.push(string2);
//     } 
//     console.log("preloading... -1")
//     preloading([...tempArr])

//   }, [])

//   function preloading (imageArray) { 
//     console.log("preloading... 0")
//     let n = imageArray.length; 
//     var completed = [];
//     for (let i = 0; i < n; i++) { 
//       let img = new Image(); 
//       img.src = imageArray[i]; 
//       completed.push("" + i)
//       if (completed.length == n) {
//         setAssetLoaded(true)
//       }
//     }
//    } 

  const playAudio = (url) => {
    console.log('[dasomtv-debug] playAudio url :', url);
    if (url && url != "") {

        const audioEl = new Audio(url.replace('https://107.167.189.37:81/tts/', '/tts/').replace('http://107.167.189.37:81/tts/', '/tts/'));
        audioEl.load();
        audioEl.muted = false;
        audioEl.volume = 1;
        audioEl.play();
    }
  };

  // [임시] 배경 클릭 시 영상 화면으로 이동
  const handleClickBackground = useCallback((e) => {
    if (e.target === avatarRef.current) { return; }
    setIsViewOfVideo(true);
  }, [avatarRef]);

  
  const showMessageAutoPlay = useCallback((message, url) => {
    console.log('[dasomtv-debug] showMessageAutoPlay url :', url);
    console.log('[dasomtv-debug] showMessageAutoPlay message :', message);

    setHasNoti(true);
    playAudio(url);
  }, []);

  useEffect(() => {
    window.addEventListener('click', handleClickBackground);
    return () => window.removeEventListener('click', handleClickBackground);
  });


  useEffect(() => {
    if (hint) {
      showMessageAutoPlay(hint, audioUrl);
    }
  }, [hint, audioUrl]);

  // 챗봇 메세지 표시
  const showMessageManul = useCallback(() => {
    setHasNoti((prev) => !prev);
    tempTtsUrl = '/tts/real/2022-03-25/2022-03-25_02_54_43_VBZZgw.mp3'; // http://107.167.189.37:81
    console.log('[dasomtv-debug] tempTtsUrl :', tempTtsUrl);
    playAudio(tempTtsUrl);
  }, []);


  return (
    <MainWrapper backgroundImage="url('/home/bg_main.png')">
      {
      /* <div className="mainAvatarBox" onClick={handleClickAvatar}>
        <img ref={avatarRef} src="/home/main_avatar.png" alt="" />
        </div> */
      }
        {
             true &&

            <div className="mainAvatarBox" 
            // onClick={showMessageManul}
            >
            {/* <img ref={avatarRef} src="/home/main_avatar.png" alt="" /> */}
              <div id="animated-background1" className="image-container">
                </div>
    
                <style jsx="true" global="true" suppressHydrationWarning>{`
                #animated-background1 {
                    animation: bgfade 4s infinite;
                    background-size: cover;
                    background-position: center;
                    background-color: transparent;
                    position: relative;
                }
                #animated-background1  {
                    // position: absolute;
                    // width: 16.4vw;
                    // bottom: 60px;
                    // z-index: 1;
    
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: 30vw;
                    height: 90vh;
                    text-align: center;
                    overflow: hidden;
                    
                }
                @keyframes bgfade {
                    0% {
                        background-image: url('/avatar2/avatar2_0.png');
                    }
                    1% {
                        background-image: url('/avatar2/avatar2_1.png')
                    }
                    2% {
                        background-image: url('/avatar2/avatar2_2.png')
                    }
                    3% {
                        background-image: url('/avatar2/avatar2_3.png')
                    }
                    4% {
                        background-image: url('/avatar2/avatar2_4.png')
                    }
                    5% {
                        background-image: url('/avatar2/avatar2_5.png');
                    }
                    6% {
                        background-image: url('/avatar2/avatar2_6.png')
                    }
                    7% {
                        background-image: url('/avatar2/avatar2_7.png')
                    }
                    8% {
                        background-image: url('/avatar2/avatar2_8.png')
                    }
                    9% {
                        background-image: url('/avatar2/avatar2_9.png')
                    }
                    10% {
                        background-image: url('/avatar2/avatar2_10.png');
                    }
                    11% {
                        background-image: url('/avatar2/avatar2_11.png')
                    }
                    12% {
                        background-image: url('/avatar2/avatar2_12.png')
                    }
                    13% {
                        background-image: url('/avatar2/avatar2_13.png')
                    }
                    14% {
                        background-image: url('/avatar2/avatar2_14.png')
                    }
                    15% {
                        background-image: url('/avatar2/avatar2_15.png');
                    }
                    16% {
                        background-image: url('/avatar2/avatar2_16.png')
                    }
                    17% {
                        background-image: url('/avatar2/avatar2_17.png')
                    }
                    18% {
                        background-image: url('/avatar2/avatar2_18.png')
                    }
                    19% {
                        background-image: url('/avatar2/avatar2_19.png')
                    }
                    20% {
                        background-image: url('/avatar2/avatar2_20.png');
                    }
                    21% {
                        background-image: url('/avatar2/avatar2_21.png')
                    }
                    22% {
                        background-image: url('/avatar2/avatar2_22.png')
                    }
                    23% {
                        background-image: url('/avatar2/avatar2_23.png')
                    }
                    24% {
                        background-image: url('/avatar2/avatar2_24.png')
                    }
                    25% {
                        background-image: url('/avatar2/avatar2_25.png');
                    }
                    26% {
                        background-image: url('/avatar2/avatar2_26.png')
                    }
                    27% {
                        background-image: url('/avatar2/avatar2_27.png')
                    }
                    28% {
                        background-image: url('/avatar2/avatar2_28.png')
                    }
                    29% {
                        background-image: url('/avatar2/avatar2_29.png')
                    }
    
                    30% {
                        background-image: url('/avatar2/avatar2_30.png');
                    }
                    31% {
                        background-image: url('/avatar2/avatar2_31.png')
                    }
                    32% {
                        background-image: url('/avatar2/avatar2_32.png')
                    }
                    33% {
                        background-image: url('/avatar2/avatar2_33.png')
                    }
                    34% {
                        background-image: url('/avatar2/avatar2_34.png')
                    }
                    35% {
                        background-image: url('/avatar2/avatar2_35.png');
                    }
                    36% {
                        background-image: url('/avatar2/avatar2_36.png')
                    }
                    37% {
                        background-image: url('/avatar2/avatar2_37.png')
                    }
                    38% {
                        background-image: url('/avatar2/avatar2_38.png')
                    }
                    39% {
                        background-image: url('/avatar2/avatar2_39.png')
                    }
                    40% {
                        background-image: url('/avatar2/avatar2_40.png');
                    }
                    41% {
                        background-image: url('/avatar2/avatar2_41.png')
                    }
                    42% {
                        background-image: url('/avatar2/avatar2_42.png')
                    }
                    43% {
                        background-image: url('/avatar2/avatar2_43.png')
                    }
                    44% {
                        background-image: url('/avatar2/avatar2_44.png')
                    }
                    45% {
                        background-image: url('/avatar2/avatar2_45.png');
                    }
                    46% {
                        background-image: url('/avatar2/avatar2_46.png')
                    }
                    47% {
                        background-image: url('/avatar2/avatar2_47.png')
                    }
                    48% {
                        background-image: url('/avatar2/avatar2_48.png')
                    }
                    49% {
                        background-image: url('/avatar2/avatar2_49.png')
                    }
    
                    50% {
                        background-image: url('/avatar2/avatar2_50.png');
                    }
                    51% {
                        background-image: url('/avatar2/avatar2_51.png')
                    }
                    52% {
                        background-image: url('/avatar2/avatar2_52.png')
                    }
                    53% {
                        background-image: url('/avatar2/avatar2_53.png')
                    }
                    54% {
                        background-image: url('/avatar2/avatar2_54.png')
                    }
                    55% {
                        background-image: url('/avatar2/avatar2_55.png');
                    }
                    56% {
                        background-image: url('/avatar2/avatar2_56.png')
                    }
                    57% {
                        background-image: url('/avatar2/avatar2_57.png')
                    }
                    58% {
                        background-image: url('/avatar2/avatar2_58.png')
                    }
                    59% {
                        background-image: url('/avatar2/avatar2_59.png')
                    }
                    60% {
                        background-image: url('/avatar2/avatar2_60.png');
                    }
                    61% {
                        background-image: url('/avatar2/avatar2_61.png')
                    }
                    62% {
                        background-image: url('/avatar2/avatar2_62.png')
                    }
                    63% {
                        background-image: url('/avatar2/avatar2_63.png')
                    }
                    64% {
                        background-image: url('/avatar2/avatar2_64.png')
                    }
                    65% {
                        background-image: url('/avatar2/avatar2_65.png');
                    }
                    66% {
                        background-image: url('/avatar2/avatar2_66.png')
                    }
                    67% {
                        background-image: url('/avatar2/avatar2_67.png')
                    }
                    68% {
                        background-image: url('/avatar2/avatar2_68.png')
                    }
                    69% {
                        background-image: url('/avatar2/avatar2_69.png')
                    }
                    70% {
                        background-image: url('/avatar2/avatar2_70.png');
                    }
                    71% {
                        background-image: url('/avatar2/avatar2_71.png')
                    }
                    72% {
                        background-image: url('/avatar2/avatar2_72.png')
                    }
                    73% {
                        background-image: url('/avatar2/avatar2_73.png')
                    }
                    74% {
                        background-image: url('/avatar2/avatar2_74.png')
                    }
                    75% {
                        background-image: url('/avatar2/avatar2_75.png');
                    }
                    76% {
                        background-image: url('/avatar2/avatar2_76.png')
                    }
                    77% {
                        background-image: url('/avatar2/avatar2_77.png')
                    }
                    78% {
                        background-image: url('/avatar2/avatar2_78.png')
                    }
                    79% {
                        background-image: url('/avatar2/avatar2_79.png')
                    }
                    80% {
                        background-image: url('/avatar2/avatar2_80.png');
                    }
                    81% {
                        background-image: url('/avatar2/avatar2_81.png')
                    }
                    82% {
                        background-image: url('/avatar2/avatar2_82.png')
                    }
                    83% {
                        background-image: url('/avatar2/avatar2_83.png')
                    }
                    84% {
                        background-image: url('/avatar2/avatar2_84.png')
                    }
                    85% {
                        background-image: url('/avatar2/avatar2_85.png');
                    }
                    86% {
                        background-image: url('/avatar2/avatar2_86.png')
                    }
                    87% {
                        background-image: url('/avatar2/avatar2_87.png')
                    }
                    88% {
                        background-image: url('/avatar2/avatar2_88.png')
                    }
                    89% {
                        background-image: url('/avatar2/avatar2_89.png')
                    }
    
                    90% {
                        background-image: url('/avatar2/avatar2_90.png');
                    }
                    91% {
                        background-image: url('/avatar2/avatar2_91.png')
                    }
                    92% {
                        background-image: url('/avatar2/avatar2_92.png')
                    }
                    93% {
                        background-image: url('/avatar2/avatar2_93.png')
                    }
                    94% {
                        background-image: url('/avatar2/avatar2_94.png')
                    }
                    95% {
                        background-image: url('/avatar2/avatar2_95.png');
                    }
                    96% {
                        background-image: url('/avatar2/avatar2_96.png')
                    }
                    97% {
                        background-image: url('/avatar2/avatar2_97.png')
                    }
                    98% {
                        background-image: url('/avatar2/avatar2_98.png')
                    }
                    99% {
                        background-image: url('/avatar2/avatar2_99.png')
                    }
                    100% {
                        background-image: url('/avatar2/avatar2_100.png');
                    }
                }
                /****** add a background overlay *******/
                #animated-background1:before {
                    content: "";
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                }
                `}</style>
          </div>
        }

      {hasNoti && (
        <DasomNotiWrapper>
          <div className="dasomNotiBox">
            <img src="/home/img_dasom_noti.png" alt="" />
            <div>{hint}</div>
          </div>
        </DasomNotiWrapper>
      )}
    </MainWrapper>
  );
};

Main.propTypes = {
  setIsViewOfVideo: PropTypes.func.isRequired,
  hint: PropTypes.string.isRequired,
  audioUrl: PropTypes.string.isRequired,
};
export default Main;
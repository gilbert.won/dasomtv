import styled from 'styled-components';

// Main
export const MainWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: ${props => props.backgroundImage};
  background-size: 100% 100%;
  background-repeat: no-repeat;
  background-position: center;

  .mainAvatarBox {
    position: absolute;
    top: 3.6vh;
    width: 100%;
    height: 96.4vh;
    text-align: center;
    overflow: hidden;
    
    img {
      width: 28.3vw;
      height: 111.2vh;
    }
  }
`;

// Dasom Notification
export const DasomNotiWrapper = styled.div`
  position: fixed;
  bottom: 60px;
  width: 100vw;
  
  .dasomNotiBox {
    width: calc(100% - 308px);
    height: auto;
    line-height: 72px;
    border-radius: 32px;
    background: rgba(255, 255, 255, 0.9);
    margin: 0 auto;
    padding: 64px;
    font-family: 'Pretendard';
    font-size: 48px;
    color: #000;

    img {
      position: absolute;
      margin-top: -112px;
    }
  }
`;
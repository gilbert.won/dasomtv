// const withPlugins = require('next-compose-plugins');

// const { i18n } = require('./next-i18next.config');

// // 빌드 시 필요
// const withBundleAnalyzer = require('@next/bundle-analyzer')({
//   enalbled: process.env.ANALYZER === 'true',
// });

// const plugins = withBundleAnalyzer({
//   compress: true,
//   webpack(config, { webpack }) {
//     const prod = process.env.NODE_ENV === 'production';
//     return {
//       ...config,
//       mode: prod ? 'production' : 'development',
//       devtool: prod ? 'hidden-source-map' : 'eval',
//     };
//   },
// });

// // reverse-proxy
// const REQUEST_URL = 'http://59.29.245.152:5000';
// const OTHER_SERVER_URL = 'http://34.107.168.113';
// const proxyConfig = {
//   async rewrites() {
//     return [
//       { source: '/ElderlyList/:path*', destination: `${REQUEST_URL}/ElderlyList/` },
//       { source: '/alarm/:path*', destination: `${OTHER_SERVER_URL}/alarm/:path*` },
//     ];
//   },
// };

// // module.exports = proxyConfig;
// module.exports = withPlugins([proxyConfig/* , plugins */], { i18n });


module.exports = {
    async rewrites() {
        return [
            {
                destination: 'https://dev.dasomi.ai/API/:path*', // process.env.DESTINATION_URL,
                source:  '/API/:path*' // process.env.SOURCE_PATH,
            },
            {
                destination: 'http://107.167.189.37:81/tts/:path*', // process.env.DESTINATION_URL,
                source:  '/tts/:path*' // process.env.SOURCE_PATH,
            },
        ];
        // if (process.env.NODE_ENV !== 'production') {
        //     return [
        //         {
        //             destination: 'https://dev.dasomi.ai/API/:path*', // process.env.DESTINATION_URL,
        //             source:  '/API/:path*' // process.env.SOURCE_PATH,
        //         },
        //     ];
        // }
    },
}
